<?php
    /*
    Template Name: Шаблон "Услуги"
    */
?>
<?php get_header(); ?>
    <div class="main__page_phone page__phone_fix">
    <div class="main__phone_text">
        <p>Мы всегда рады прийти Вам на помощь</p>
        <h2>+7 (495) 120-31-69</h2>
    </div>
</div>

<?php if (have_posts()): while (have_posts()): the_post(); ?>
    <?php the_content(); ?>
<?php endwhile; endif; ?>
<div class="mainbanner" style="margin-bottom: 0px">
    <div class="portfolio-row-half">
        <div class="portfolio-grid-item-color">
            <div class="desc">
                <h2>Мы эффективно уничтожим</h2>
                <p class="grid__portfolio_text">Гарантированно уничножим всех Ваших врагов</p>
                <p><a href="/contacts/" class="btn btn-primary btn-outline with-arrow">Заказать<i class="icon-arrow-right22"></i></a></p>
            </div>
        </div>
        <a href="/uslugi/deratizaciya/" class="portfolio-grid-item" style="background-image: url(/wp-content/uploads/2017/04/uslugi-mouse.jpg);">
            <div class="desc2">
                <h3>Грызуны</h3>
                <span>Дератизация: мышей, крыс, кротов</span>
                <i class="sl-icon-heart"></i>
            </div>
        </a>
        <a href="/uslugi/dezinsekciya/" class="portfolio-grid-item" style="background-image: url(/wp-content/uploads/2017/04/uslugi-klop.jpg);">
            <div class="desc2">
                <h3>Насекомые</h3>
                <span>Дезинсекция: клопов, тараканов, моли, ос</span>
                <i class="sl-icon-heart"></i>
            </div>
        </a>
        <a href="/uslugi/" class="portfolio-grid-item" style="background-image: url(/wp-content/uploads/2017/04/uslugi-hazop.jpg);">
            <div class="desc2">
                <h3>Объекты любой сложности</h3>
                <span>Квартиры, коттеджи и предприятия</span>
                <i class="sl-icon-heart"></i>
            </div>
        </a>
    </div>
</div>


<?php get_footer(); ?>
