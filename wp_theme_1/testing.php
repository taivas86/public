<div class="ourservices">
    <div class="ourservices__headblock">
        <h2 class="ourservices__headblock_header">Наши услуги</h2>
        <div class="ourservices__headblock_text">
            <div class="ourservices__text">
                <p>Наша компания предоставляет полный цикл услуг по уничтожению бактерий и грибков, насекомых, грызунов и дезинфекции помещений. Мы работаем со всеми видами заказчиков, как частными квартирами и домохозяйствами, так и предприятиями общественного питания и пищевыми производствами</p>
            </div>
        </div>

    </div>
    <div class="ourservices__blocks">
        <div class="ourservices__block_common">
            <div class="otherservices__icon_block">
                <div class="ourservices__icon">
                    <img src="wp-content/uploads/2017/03/laboratory.png">
                </div>
            </div>
            <div class="otherservices__block_text">
                <h3 class="otherservices__block_header">Дезинфекция</h3>
                <p>Дезинфекция - уничтожение микроорганизмов и возбудителей заболеваний, плесени и грибков в жилых помещениях и производстве. Мы выполняем все виды дезинфекции в Москве и Московской области: профилактическую, текущую, и заключительную</p>
            </div>
        </div>
        <div class="ourservices__block_common">
            <div class="otherservices__icon_block">
                <div class="ourservices__icon">
                    <img src="wp-content/uploads/2017/03/bee.png">
                </div>
            </div>
            <div class="otherservices__block_text">
                <h3 class="otherservices__block_header">Дезинсекция</h3>
                <p>Дезинсекция - уничтожение насекомых с помощью технических, химических и биологических средств. Полное истребление насекомых с помощью безопасных для человека инсектицидов и репеллентов</p>
            </div>
        </div>
        <div class="ourservices__block_common">
            <div class="otherservices__icon_block">
                <div class="ourservices__icon">
                    <img src="wp-content/uploads/2017/03/mouse.png">
                </div>
            </div>
            <div class="otherservices__block_text">
                <h3 class="otherservices__block_header">Дератизация</h3>
                <p>Дератизация - уничтожение крыс, мышей и прочих грызунов. Качественная обработка жилых и производственных помещений в Москве и Московской области с применением специальных контейнеров и оформлением документации для СЭС</p>
            </div>
        </div>

    </div>
</div>
<div class="mainbanner">
    <div class="portfolio-row-half">
        <div class="portfolio-grid-item-color">
            <div class="desc">
                <h2>Мы эффективно уничтожим</h2>
                <p class="grid__portfolio_text">Гарантированно уничножим всех Ваших врагов</p>
                <p><a href="/kontakty/" class="btn btn-primary btn-outline with-arrow">Заказать<i class="icon-arrow-right22"></i></a></p>
            </div>
        </div>
        <a href="/uslugi/dezinfekciya/" class="portfolio-grid-item" style="background-image: url(/wp-content/uploads/2017/03/viruswp.jpg);">
            <div class="desc2">
                <h3>Дезинфекция</h3>
                <span>Уничтожение микроорганизмов</span>
                <i class="sl-icon-heart"></i>
            </div>
        </a>
        <a href="/uslugi/deratizaciya/" class="portfolio-grid-item" style="background-image: url(/wp-content/uploads/2017/03/mousewp.jpg);">
            <div class="desc2">
                <h3>Дератизация</h3>
                <span>Уничтожение грызунов</span>
                <i class="sl-icon-heart"></i>
            </div>
        </a>
        <a href="/uslugi/dezinsekciya/" class="portfolio-grid-item" style="background-image: url(/wp-content/uploads/2017/03/beewp.jpg);">
            <div class="desc2">
                <h3>Дезинсекция</h3>
                <span>Уничтожение насекомых</span>
                <i class="sl-icon-heart"></i>
            </div>
        </a>
    </div>
</div>
<div class="ourprices">
    <div class="ourprices__image">
        <img src="/wp-content/uploads/2017/03/accessory.png">
    </div>
    <div class="ourprices__head">
        <h3 class="ourprices__header">Наши цены</h3>
        <div class="ourprices__head_text">
            <div class="outprices__header_text">
                <p>Наши специалисты готовы приехать к Вам в любое удобное для Вас время. Мы применяем только сертифицированные препараты, которые с применением современных технологий, позволяют сократить время воздействия препарата на человека и окружающую среду. "Сандер Гарант" гарантирует Вам результат и конфиденциальность</p>
            </div>
        </div>
    </div>
    <div class="ourprices__blocks">
        <div class="ourprices__block_common">
            <h3 class="ourprices__common_head"><strong>Дезинфекция</strong></h3>
            <div class="outprices__common_currency">
                <sup class="outprices__currency">&#8381;</sup>2700
            </div>
            <div class="outprices__common_desc">
                <p>Уничтожение плесени</p>
            </div>
            <hr class="outprices__divider">
            <ul class="outprices__common_list">
                <li class="outprices__list_padding">Бактерии</li>
                <li class="outprices__list_padding">Останки</li>
                <li class="outprices__list_padding">Загрязнения</li>
                <li class="outprices__list_padding">Уборка</li>
            </ul>
            <div class="ourprices__block_button">
                <a href="/uslugi/dezinfekciya/" class="btn btn-primary outprices__button_spec">Узнать больше</a>
            </div>
        </div>
        <div class="ourprices__block_common ourprices__ipad_fix">
            <h3 class="ourprices__common_head"><strong>Дезинсекция</strong></h3>
            <div class="outprices__common_currency">
                <sup class="outprices__currency">&#8381;</sup>2400
            </div>
            <div class="outprices__common_desc">
                <p>Уничтожение насекомых</p>
            </div>
            <hr class="outprices__divider">
            <ul class="outprices__common_list">
                <li class="outprices__list_padding">Тараканы</li>
                <li class="outprices__list_padding">Клопы</li>
                <li class="outprices__list_padding">Осы и пчелы</li>
                <li class="outprices__list_padding">Клещи</li>
            </ul>
            <div class="ourprices__block_button">
                <a href="/uslugi/dezinsekciya/" class="btn btn-primary outprices__button_spec">Узнать больше</a>
            </div>
        </div>
        <div class="ourprices__block_common">
            <div class="salebox">
                <div class="outprices__sale">
                    <div class="outprices__sale_box">
                        <div>
                            <p class="outprices__box_textcolor">Выгодно</p>
                        </div>
                    </div>
                </div>
                <div class="outprices__sale_triangle">
                    <span class="glyphicon glyphicon-triangle-bottom"></span>
                </div>
            </div>
            <div class="ourprices__block_sale"">
            <h3 class="ourprices__common_head"><strong>Дератизация</strong></h3>
            <div class="outprices__common_currency">
                <sup class="outprices__currency">&#8381;</sup>5000
            </div>
            <div class="outprices__common_desc">
                <p>Уничтожение грызунов</p>
            </div>
            <hr class="outprices__divider">
            <ul class="outprices__common_list">
                <li class="outprices__list_padding">Крысы</li>
                <li class="outprices__list_padding">Мыши</li>
                <li class="outprices__list_padding">Кроты</li>
                <li class="outprices__list_padding">Прочие грызуны</li>
            </ul>
            <div class="ourprices__block_button">
                <a href="/uslugi/deratizaciya/" class="btn btn-primary outprices__button_spec">Узнать больше</a>
            </div>
        </div>
    </div>
</div>
</div>
