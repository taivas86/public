<?php
/*
Template Name: Шаблон "Контакты".
*/
?>

<?php get_header()?>

    <div id="map"></div>
    <script type="text/javascript">
        var office= {lat:55.819377, lng:37.577579};

        var map;
        function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
                scrollwheel:  false,
                center: {lat:55.819377, lng:37.577579},
                zoom: 17,
                mapTypeControl: false
            });

            var marker = new google.maps.Marker({
                position: office,
                map: map,
                title: 'Hello World!'
            });

        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD52BDh3xPbfYYvPQzfBH2Z_2jJnKjkvyM&callback=initMap">
    </script>


<?php if (have_posts()): while (have_posts()): the_post(); ?>
    <?php the_content(); ?>
<?php endwhile; endif; ?>

<?php get_footer() ?>

