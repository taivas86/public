<!doctype html>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <title><?php wp_title('«', true, 'right'); ?> <?php bloginfo('name'); ?></title>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700" rel="stylesheet">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <nav class="navbar navbar-default navbar__specs navbar-fixed-top">
        <?php
            // Fix menu overlap
            if ( is_admin_bar_showing() ) echo '<div class="fix_wp_overlap"></div>';
        ?>
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand navbar__logo" href="/">Сандер Гарант</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <!--Услуги-->
                    <li class="dropdown navbar__link_focus" >
                        <a href="/uslugi/" class="dropdown-toggle disabled nav__links_specs" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Услуги<span class="caret mobile__disable_caret"></span></a>
                        <ul class="dropdown-menu dropdown-menu-left">
                            <li><a class="navbar__dropdown_specs" href="/uslugi/dezinsekciya/">Деинсекция</a></li>
                            <li><a class=navbar__dropdown_specs" href="/uslugi/deratizaciya/">Дератизация</a></li>
                            <li><a class=navbar__dropdown_specs" href="/uslugi/dezinfekciya/">Дезинфекция</a></li>

                        </ul>
                    </li>

                    <!--Для предприятий-->
                    <li class="dropdown navbar__link_focus">
                        <a href="/dlya-predpriyatij/" class="dropdown-toggle disabled nav__links_specs" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Для предприятий<span class="caret mobile__disable_caret"></span></a>
                        <ul class="dropdown-menu dropdown-menu-left">
                            <li><a href="/dlya-predpriyatij/sklad/">Склад</a></li>
                            <li><a href="/dlya-predpriyatij/gostinica/">Гостиница</a></li>
                            <li><a href="/dlya-predpriyatij/obshchepit/">Общепит</a></li>
                            <li><a href="/dlya-predpriyatij/proizvodstvо/">Производство</a></li>
                            <li><a href="/dlya-predpriyatij/zhivotnovodstvo/">Животноводство</a></li>
                        </ul>
                    </li>
                    <!--Цены-->
                    <li class="navbar__link_focus"><a href="/tseny/" class="nav__links_specs">Цены</a></li>

                    <!--Контакты-->
                    <li class="dropdown navbar__link_focus">
                        <a href="/contacts/" class="dropdown-toggle disabled nav__links_specs" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Контакты<span class="caret mobile__disable_caret"></span></a>
                        <ul class="dropdown-menu dropdown-menu-left">
                            <li><a href="/contacts/nash-blog/">Блог</a></li>
                            <li><a href="/contacts/sertifikaty/">Сертификаты</a></li>
                            <li><a href="/contacts/o-kompanii/">О компании</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
