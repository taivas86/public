<?php
/*
Template Name: Шаблон "Страница"
*/
?>

<?php get_header(); ?>
<div class="main__page_phone page__phone_fix">
    <div class="main__phone_text">
        <p>Гарантированное уничтожение насекомых и грызунов в Москве и Московской области</p>
        <h2>+7 (495) 120-31-69</h2>
    </div>
</div>
<div class="page__content">
    <?php if (have_posts()): while (have_posts()): the_post(); ?>
        <div class="page__content_header">
            <h1><?php the_title(); ?></h1>
        </div>
        <div class="page__content_text">
             <?php the_content(); ?>
        </div>
    <?php endwhile; endif; ?>
</div>
<?php get_footer(); ?>



