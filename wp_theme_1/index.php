<?php
/*
Template Name: Шаблон страницы "Блог"
*/
?>
<?php
/*
Шаблон страницы "Блог"
*/
?>
<?php get_header(); ?>
    <div class="blog">
        <h2 class="blog__header">Актуальные новости и интересные статьи</h2>
        <div class="gridcontainer">
            <?php
            // Grid Parameters
            $counter = 1; // Start the counter
            $grids = 2; // Grids per row
            $titlelength = 50; // Length of the post titles shown below the thumbnails

            // The Loop
            query_posts($query_string.'&cat=2&showposts=4');
            while (have_posts()) : the_post();
                // Show all columns except the right hand side column
                if ($counter != $grids) :
                    ?>
                    <div class="griditemleft">
                        <div class="griditemleft__flex">
                            <h3 class="postimage-title main__blog_title">
                                <a class="blog__links_settings"href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                    <?php
                                    if (mb_strlen($post->post_title) > $titlelength) {
                                        echo mb_substr(the_title($before = '', $after = '', FALSE), 0, $titlelength) . ' ...';
                                    } else {
                                        the_title();
                                    }
                                    ?>
                                </a>
                            </h3>
                            <div class="blog__next">
                                <a href="<?php the_permalink(); ?>" class="btn btn-primary outprices__button_spec blog__button">Читать далее</a>
                            </div>
                        </div>
                        <div class="postimage">
                            <a href="<?php the_permalink(); ?>"
                               title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail(); ?>
                            </a>
                        </div><!-- .postimage -->
                    </div>
                    <!-- end griditemleft -->
                    <?php
                // Show the right hand side column
                elseif ($counter == $grids) :
                    ?>
                    <div class="griditemright">
                        <div class="griditemright__flex">
                            <h3 class="postimage-title main__blog_title">
                                <a class="blog__links_settings" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                    <?php
                                    if (mb_strlen($post->post_title) > $titlelength) {
                                        echo mb_substr(the_title($before = '', $after = '', FALSE), 0, $titlelength) . ' ...';
                                    } else {
                                        the_title();
                                    }
                                    ?>
                                </a>
                            </h3>
                            <div class="blog__next">
                                <a href="<?php the_permalink(); ?>" class="btn btn-primary outprices__button_spec blog__button">Читать далее</a>
                            </div>
                        </div>
                        <div class="postimage">
                            <a href="<?php the_permalink(); ?>"
                               title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail(); ?>
                            </a>
                        </div><!-- .postimage -->
                    </div><!-- .griditemright -->
                    <div class="clear"></div>
                    <?php
                    $counter = 0;
                endif;
                $counter++;
            endwhile;
            wp_reset_postdata();
            ?>
        </div>
        <div style="clear:both"></div>
        <div class="blog__pagination">
            <?php wp_pagenavi(); ?>
        </div>
    </div>
<?php get_footer(); ?>
