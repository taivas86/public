$(document).ready(function () {
    var mySwiper = new Swiper ('.swiper-container', {
        autoplay: 3000,
        autoplayDisableOnInteraction: false,
        loop: true,
        speed:4000,
        simulateTouch:false
    })
});
