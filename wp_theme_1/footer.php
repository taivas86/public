<footer>
    <div class="footer">
        <div class="footer__commonblock">
            <div class="footer__commonblock_list">
                <p>© <?php echo date('Y'); ?>  "Сандер Гарант"</p>
                <p>Адрес: 127332, г. Москва, Яблочкова, д.21, к.3</p>
                <p>Телефон: +7 (495) 120-31-69</p>
                <p>E-mail: <a href="mailto:sandergarant@gmail.com ">sandergarant@gmail.com</a></p>
            </div>
        </div>
        <div class="footer__commonblock footer__commonblock_second">
            <p><bold>О компании</bold></p>
            <ul class="footer__commonblock_list">
                <p><a href="/uslugi/">Услуги</a></p>
                <p><a href="/dlya-predpriyatij/">Для предприятий</a></p>
                <p><a href="/tseny/">Цены</a></p>
                <p><a href="/contacts/o-kompanii/">О компании</a></p>
                <p><a href="/contacts/">Контакты</a></p>
                <p><a href="/sertifikaty/">Сертификаты</a></p>
            </ul>
        </div>
        <div class="footer__commonblock">
            <h4>Дополнительная информация</h4>
            <p>Цены на сайте носят информационный характер и не являются публичной офертой</p>
        </div>
    </div>
    <div class="our__brand">
        <a href="https://outlooker.ru">Разработка сайта Outlooker.ru</a>
    </div>
<!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter44019504 = new Ya.Metrika({ id:44019504, clickmap:true, trackLinks:true, accurateTrackBounce:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/44019504" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
</footer>
<?php wp_footer(); ?>
</body>
</html>
