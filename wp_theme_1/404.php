<?php get_header()?>
<div class="main__page_phone page__phone_fix">
    <div class="main__phone_text">
        <p>Мы всегда рады прийти Вам на помощь</p>
        <h2>+7 (495) 120-31-69</h2>
    </div>
</div>
<h1 class="error__header">Ошибка 404</h1>
<h3 class="error__header_second">Мы меняемся вместе с Вами</h3>
<div class="mainbanner" style="margin-bottom: 0px">
    <div class="portfolio-row-half">
        <div class="portfolio-grid-item-color">
            <div class="desc">
                <h2>Мы эффективно уничтожим</h2>
                <p class="grid__portfolio_text">Гарантированно уничножим всех Ваших врагов</p>
                <p><a href="/kontakty/" class="btn btn-primary btn-outline with-arrow">Заказать<i class="icon-arrow-right22"></i></a></p>
            </div>
        </div>
        <a href="/uslugi/dezinfekciya/" class="portfolio-grid-item" style="background-image: url(/wp-content/uploads/2017/03/viruswp.jpg);">
            <div class="desc2">
                <h3>Дезинфекция</h3>
                <span>Уничтожение микроорганизмов</span>
                <i class="sl-icon-heart"></i>
            </div>
        </a>
        <a href="/uslugi/deratizaciya/" class="portfolio-grid-item" style="background-image: url(/wp-content/uploads/2017/03/mousewp.jpg);">
            <div class="desc2">
                <h3>Дератизация</h3>
                <span>Уничтожение грызунов</span>
                <i class="sl-icon-heart"></i>
            </div>
        </a>
        <a href="/uslugi/dezinsekciya/" class="portfolio-grid-item" style="background-image: url(/wp-content/uploads/2017/03/beewp.jpg);">
            <div class="desc2">
                <h3>Дезинсекция</h3>
                <span>Уничтожение насекомых</span>
                <i class="sl-icon-heart"></i>
            </div>
        </a>
    </div>
</div>
<?php get_footer() ?>


