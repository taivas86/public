<?php
/*
Template Name: Шаблон "Дезинсекция"
*/
?>

<?php get_header(); ?>
<div class="main__page_phone page__phone_fix">
    <div class="main__phone_text">
        <p>Гарантированное уничтожение насекомых и грызунов в Москве и Московской области</p>
        <h2>+7 (495) 120-31-69</h2>
    </div>
</div>
<div class="page__content">
    <?php if (have_posts()): while (have_posts()): the_post(); ?>
        <div class="page__content_header">
            <h1><?php the_title(); ?></h1>
        </div>
        <div class="page__content_text">
             <?php the_content(); ?>
        </div>
    <?php endwhile; endif; ?>
</div>
<div class="mainbanner" style="margin-bottom: 0px">
    <div class="portfolio-row-half">
        <div class="portfolio-grid-item-color">
            <div class="desc">
                <h2>Мы эффективно уничтожим</h2>
                <p class="grid__portfolio_text">Гарантированно уничножим всех Ваших врагов</p>
                <p><a href="/contacts/" class="btn btn-primary btn-outline with-arrow">Заказать<i class="icon-arrow-right22"></i></a></p>
            </div>
        </div>
        <a href="/uslugi/" class="portfolio-grid-item" style="background-image: url(/wp-content/uploads/2017/04/uslugi-roach.jpg);">
            <div class="desc2">
                <h3>Уничтожение тараканов</h3>
                <i class="sl-icon-heart"></i>
            </div>
        </a>
        <a href="/uslugi/" class="portfolio-grid-item" style="background-image: url(/wp-content/uploads/2017/04/uslugi-klop.jpg);">
            <div class="desc2">
                <h3>Уничтожение клопов</h3>
                <i class="sl-icon-heart"></i>
            </div>
        </a>
        <a href="/uslugi/" class="portfolio-grid-item" style="background-image: url(/wp-content/uploads/2017/04/uslugi-mol.jpg);">
            <div class="desc2">
                <h3>Уничтожение моли</h3>
                <i class="sl-icon-heart"></i>
            </div>
        </a>
    </div>
</div>
<?php get_footer(); ?>
