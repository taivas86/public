<?php
/*
Template Name: Шаблон главной страницы
*/
?>
<?php get_header(); ?>
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide slider__slide_one">
                <div class="slider__content_center">
                    <div class="slider__content">
                        <div class="slider__text_flex">
                            <h3>Безопасные технологии дезинфекции и дератизации</h3>
                        </div>
                        <div class="slider__button">
                            <a class="slider__button_specs btn btn-primary" href="/uslugi/">Узнайте больше</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide slider__slide_two">
                <div class="slider__content_center">
                    <div class="slider__content">
                        <div class="slider__text_flex">
                            <h1>Дезинфекция и дератизация для кафе и ресторанов</h1>
                        </div>
                        <div class="slider__button">
                            <a class="slider__button_specs btn btn-primary"" href="/dlya-predpriyatij/obshchepit/">Узнайте больше</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide slider__slide_three">
                <div class="slider__content_center">
                    <div class="slider__content">
                        <div class="slider__text_flex">
                            <h3>Многолетний опыт сотрудничества с предприятями</h3>
                        </div>
                        <div class="slider__button">
                            <a class="slider__button_specs btn btn-primary"" href="/dlya-predpriyatij/">Узнайте больше</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
    </div>
    <div class="main__page_phone">
        <div class="main__phone_text">
            <p>Мы всегда рады прийти Вам на помощь</p>
            <h2>+7 (495) 120-31-69</h2>
        </div>
    </div>
    <?php if (have_posts()): while (have_posts()): the_post(); ?>
            <?php the_content(); ?>
        <?php endwhile; endif; ?>
    <div class="clientreview">
        <h3 class="clientreview__header">Свежие новости</h3>
        <div class="gridcontainer clientreview__blog">
            <?php
            // Grid Parameters
            $counter = 1; // Start the counter
            $grids = 1; // Grids per row
            $titlelength = 50; // Length of the post titles shown below the thumbnails

            // The Loop
            query_posts($query_string.'&cat=2&showposts=2');
            while (have_posts()) : the_post();
                // Show all columns except the right hand side column
                if ($counter != $grids) :
                    ?>
                    <div class="griditemleft">
                        <div class="griditemleft__flex">
                            <h3 class="postimage-title main__blog_title">
                                <a class="blog__links_settings"href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                    <?php
                                    if (mb_strlen($post->post_title) > $titlelength) {
                                        echo mb_substr(the_title($before = '', $after = '', FALSE), 0, $titlelength) . ' ...';
                                    } else {
                                        the_title();
                                    }
                                    ?>
                                </a>
                            </h3>
                            <div class="blog__next">
                                <a href="<?php the_permalink(); ?>" class="btn btn-primary outprices__button_spec blog__button">Читать далее</a>
                            </div>
                        </div>
                        <div class="postimage">
                            <a href="<?php the_permalink(); ?>"
                               title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail(); ?>
                            </a>
                        </div><!-- .postimage -->
                    </div>
                    <!-- end griditemleft -->
                    <?php
                // Show the right hand side column
                elseif ($counter == $grids) :
                    ?>
                    <div class="griditemright">
                        <div class="griditemright__flex">
                            <h3 class="postimage-title main__blog_title">
                                <a class="blog__links_settings" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                    <?php
                                    if (mb_strlen($post->post_title) > $titlelength) {
                                        echo mb_substr(the_title($before = '', $after = '', FALSE), 0, $titlelength) . ' ...';
                                    } else {
                                        the_title();
                                    }
                                    ?>
                                </a>
                            </h3>
                            <div class="blog__next">
                                <a href="<?php the_permalink(); ?>" class="btn btn-primary outprices__button_spec blog__button">Читать далее</a>
                            </div>
                        </div>
                        <div class="postimage">
                            <a href="<?php the_permalink(); ?>"
                               title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail(); ?>
                            </a>
                        </div><!-- .postimage -->
                    </div><!-- .griditemright -->
                    <div class="clear"></div>
                    <?php
                    $counter = 0;
                endif;
                $counter++;
            endwhile;
            wp_reset_postdata();
            ?>
        </div>
    </div>
    <div id="fh5co-counter-section" class="fh5co-counters">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
                    <i class="sl-icon-badge"></i>
                    <h2 class="counter__header">Наши достижения</h2>
                    <p class="counter__header_p">Мы готовы взяться за выполнение работ любой сложности</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 text-center">
                    <span class="fh5co-counter js-counter" data-from="0" data-to="105000" data-speed="3700" data-refresh-interval="75"></span>
                    <span class="fh5co-counter-label">Насекомые</span>
                </div>
                <div class="col-md-3 text-center">
                    <span class="fh5co-counter js-counter" data-from="0" data-to="1700" data-speed="5000" data-refresh-interval="50"></span>
                    <span class="fh5co-counter-label">Грызуны</span>
                </div>
                <div class="col-md-3 text-center">
                    <span class="fh5co-counter js-counter" data-from="0" data-to="1382" data-speed="5000" data-refresh-interval="50"></span>
                    <span class="fh5co-counter-label">Обработно объектов</span>
                </div>
                <div class="col-md-3 text-center">
                    <span class="fh5co-counter js-counter" data-from="0" data-to="753" data-speed="5000" data-refresh-interval="50"></span>
                    <span class="fh5co-counter-label">Довольные клиенты</span>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>
