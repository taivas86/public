<?php
/*
Template Name: Шаблон "Для предприятий"
*/
?>

<?php get_header(); ?>
<div class="main__page_phone page__phone_fix">
    <div class="main__phone_text">
        <p>Мы всегда рады прийти Вам на помощь</p>
        <h2>+7 (495) 120-31-69</h2>
    </div>
</div>

<?php if (have_posts()): while (have_posts()): the_post(); ?>
    <?php the_content(); ?>
<?php endwhile; endif; ?>



<?php get_footer(); ?>

