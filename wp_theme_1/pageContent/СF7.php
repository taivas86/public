<div class="contactpage__form">
    <div class="contactpage__info">
        <h3 class="contact__page_header">Наш адрес</h3>
        <p class="contact__page_text">Профессиональное уничтожение болезней, насекомых и грузынов в Москве и Подмосковье</p>
        <div class="contact__page_list">
            <p><span class="glyphicon glyphicon-road glypcontacts"><i class="icon-location-pin"></i></span>127332, г. Москва, Яблочкова, д.21, к.3</p>
            <p><span class="glyphicon glyphicon-phone glypcontacts"><i class="icon-phone2"></i></span>Телефон: +7 (495) 120-31-69</p>
            <p><span class="glyphicon glyphicon-envelope glypcontacts"><i class="icon-mail"></i></span>E-mail: <a href="mailto:sandergarant@gmail.com ">sandergarant@gmail.com</a></p>
            <p><span class="glyphicon glyphicon-globe glypcontacts"><i class="icon-globe2"></i></span>Веб-сайт: <a href="/">www.yoursite.com</a></p>
        </div>
    </div>
    <div class="contactpage__input">
        <h3 class="contact__page_header">Связаться с нами</h3>
        <div class="confactpage__line_item"><p class="contactpage__input_p">Ваше имя:</p></div>
        <div class="contactpage__line_input">
            <div class="form-group">
                <input type="text" class="form-control"/>
            </div>
        </div>
        <div class="contactpage__line_item"><p class="contactpage__input_p">Ваш e-mail</p></div>
        <div class="contactpage__line_input">
            <div class="form-group">
                <input type="text" class="form-control"/>
            </div>
        </div>
        <div class="contactpage__line_item"><p class="contactpage__input_p">Ваш телефон</p></div>
        <div class="contactpage__line_input">
            <div class="form-group">
                <input type="text" class="form-control"/>
            </div>
        </div>
        <div class="contactpage__line_item"><p class="contactpage__input_p">Ваше сообщение</p></div>
        <div class="contactpage__line_input">
            <div class="form-group">
                <textarea class="form-control" rows="4"></textarea>
            </div>
        </div>
        <div class="contactcaptcha">
            <div class="contactcaptcha_flex">
                <div class="contactcaptcha_field">
                    <p class="contactpage__captcha_p">Введите защитный код:</p>
                </div>
                <div class="contactpage__line_input">
                    <div class="input-field">
                        [captchar captcha-115 class:contactpage__captcha_input]
                    </div>
                </div>
            </div>
            <div class="contactcaptcha__img">
                [captchac captcha-115]
            </div>
        </div>
        [submit class:btn-primary class:btn class:outprices__button_spec "Отправить сообщение"]
    </div>
</div>