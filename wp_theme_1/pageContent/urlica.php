<div class="ourservices factory">
    <div class="ourservices__headblock">
        <h2 class="ourservices__headblock_header">Услуги для предприятий</h2>
        <div class="ourservices__headblock_text">
            <div class="ourservices__text factory__header_text">
                <p>Основные потребители наших услуг - корпоративные заказчики. Наш опыт позволяет с уверенностью браться за решение задач любой сложности. Мы также понимаем особенности ведения бизнеса и гарантируем качество при соблюдении конфиденциальности оказания услуг. Стоимость работ рассчитывается за 1 квадратный метр, <bold>начальная стоимость работ от 25р за м2</bold>, но не менее 100 м2</p>
            </div>
        </div>
    </div>
    <div class="ourservices__blocks factory__block_one">
        <div class="ourservices__block_common factory__block_common">
            <div class="otherservices__icon_block">
                <div class="ourservices__icon">
                    <img src="/wp-content/uploads/2017/03/sklad.png">
                </div>
            </div>
            <div class="otherservices__block_text factory__block_text">
                <h3 class="otherservices__block_header factory__links"><a href="/dlya-predpriyatij/sklad/">Склад</a></h3>
                <p class="factory__text_spec">Дезинсекция и дератизация складов продовольствия и материальной части</p>
            </div>
        </div>
        <div class="ourservices__block_common factory__block_common">
            <div class="otherservices__icon_block">
                <div class="ourservices__icon">
                    <img src="/wp-content/uploads/2017/03/hotel.png">
                </div>
            </div>
            <div class="otherservices__block_text factory__block_text">
                <h3 class="otherservices__block_header factory__links"><a href="/dlya-predpriyatij/gostinica/">Гостиница</a></h3>
                <p class="factory__block_text">Дезинфекция и деинсекция номеров гостиничных комплексов и хостелов</p>
            </div>
        </div>
        <div class="ourservices__block_common factory__block_common">
            <div class="otherservices__icon_block">
                <div class="ourservices__icon">
                    <img src="/wp-content/uploads/2017/03/obshepit.png">
                </div>
            </div>
            <div class="otherservices__block_text factory__block_text">
                <h3 class="otherservices__block_header factory__links"><a href="/dlya-predpriyatij/obshchepit/">Общепит</a></h3>
                <p class="factory__block_text">Дезинфекция кухонного оборудования. Дезинсекция и дератизация помещений кафе и ресторанов</p>
            </div>
        </div>
    </div>
    <br/>
    <div class="ourservices__blocks">
        <div class="ourservices__block_common factory__block_common">
            <div class="otherservices__icon_block">
                <div class="ourservices__icon">
                    <img src="/wp-content/uploads/2017/03/factory.png">
                </div>
            </div>
            <div class="otherservices__block_text factory__block_text">
                <h3 class="otherservices__block_header factory__links"><a href="/dlya-predpriyatij/proizvodstvо/">Производство</a></h3>
                <p class="factory__text_spec">Полный спект услуг санэпидемслужбы для производственных предприятий</p>
            </div>
        </div>
        <div class="ourservices__block_common factory__block_common">
            <div class="otherservices__icon_block">
                <div class="ourservices__icon">
                    <img src="/wp-content/uploads/2017/03/farm.png">
                </div>
            </div>
            <div class="otherservices__block_text factory__block_text">
                <h3 class="otherservices__block_header factory__links"><a href="/dlya-predpriyatij/zhivotnovodstvo/">Животноводство</a></h3>
                <p class="factory__block_text">Дезинсекция, дератизация ферм и животноводческих предприятий</p>
            </div>
        </div>
        <div class="ourservices__block_common factory__block_common">
            <div class="otherservices__icon_block">
                <div class="ourservices__icon">
                    <img src="/wp-content/uploads/2017/03/circus.png">
                </div>
            </div>
            <div class="otherservices__block_text factory__block_text">
                <h3 class="otherservices__block_header factory__links"><a href="/contacts">Прочее</a></h3>
                <p class="factory__block_text">Не нашли свою организацию в списке? Не надо отчаиваться, позвоните нам</p>
            </div>
        </div>
    </div>
</div>