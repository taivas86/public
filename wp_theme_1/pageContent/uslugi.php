<div class="ourservices__headblock uslugi__page_head">
    <div class="ourservices__headblock_text">
        <div class="ourservices__text uslugi__seo_text">
            <p>Мы предалагаем услуги по дезинфекции, дератизации и дезинсекции для владельцев кватир и коттеджей. Мы работаем удобное для Вас время и стараемся применять безвредные для здоровья и экологии химикаты</p>
        </div>
    </div>
</div>
<div class="uslugi__page_seo">
    <h3>Стоимость оказания услуг</h3>
    <div class="uslugi__table_flex">
        <table class="table table-striped uslugi__page_table">
            <tbody>
            <tr>
                <td class="success table__padding">Вид обработки</td>
                <td class="success table__padding">Стоимость</td>
            </tr>
            <tr>
                <td class="table__padding"><a href="/uslugi/dezinfekciya/">Дезинфекция</a></td>
                <td class="table__padding">от 3500 ₽</td>
            </tr>
            <tr>
                <td class="table__padding"><a href="/uslugi/dezinsekciya/">Дезинсекция</a></td>
                <td class="table__padding">от 5600 ₽</td>
            </tr>
            <tr>
                <td class="table__padding"><a href="/uslugi/deratizaciya/">Дератизация</td>
                <td class="table__padding">от 15999 ₽</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="ourservices uslugi__page">
    <div class="ourservices__blocks">
        <div class="ourservices__block_common">
            <div class="otherservices__icon_block">
                <div class="ourservices__icon">
                    <img src="wp-content/uploads/2017/03/laboratory.png">
                </div>
            </div>
            <div class="otherservices__block_text">
                <h3 class="otherservices__block_header"><a href="/uslugi/dezinfekciya/">Дезинфекция</a></h3>
                <p>Наша компания предоставляет полный цикл услуг уничтожения бактерий, насекомых, грызунов и дезинфекции помещения. Мы работаем со всеми видами заказчиков, как домохозяйствами, так и предприятиями общественного питания и пиществых производств</p>
            </div>
        </div>
        <div class="ourservices__block_common">
            <div class="otherservices__icon_block">
                <div class="ourservices__icon">
                    <img src="wp-content/uploads/2017/03/bee.png">
                </div>
            </div>
            <div class="otherservices__block_text">
                <h3 class="otherservices__block_header"><a href="/uslugi/dezinsekciya/">Дезинсекция</a></h3>
                <p>Наша компания предоставляет полный цикл услуг уничтожения бактерий, насекомых, грызунов и дезинфекции помещения. Мы работаем со всеми видами заказчиков, как домохозяйствами, так и предприятиями общественного питания и пиществых производств</p>
            </div>
        </div>
        <div class="ourservices__block_common">
            <div class="otherservices__icon_block">
                <div class="ourservices__icon">
                    <img src="wp-content/uploads/2017/03/mouse.png">
                </div>
            </div>
            <div class="otherservices__block_text">
                <h3 class="otherservices__block_header"><a href="/uslugi/deratizaciya/">Дератизация</a></h3>
                <p>Наша компания предоставляет полный цикл услуг уничтожения бактерий, насекомых, грызунов и дезинфекции помещения. Мы работаем со всеми видами заказчиков, как домохозяйствами, так и предприятиями общественного питания и пиществых производств</p>
            </div>
        </div>
    </div>
</div>
