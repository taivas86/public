<div class="ourservices">
        <div class="ourservices__headblock">
            <h2 class="ourservices__headblock_header">Наши услуги</h2>
            <div class="ourservices__headblock_text">
                <div class="ourservices__text">
                <p>Наша компания предоставляет полный цикл услуг уничтожения бактерий, насекомых, грызунов и дезинфекции помещения. Мы работаем со всеми видами заказчиков, как домохозяйствами, так и предприятиями общественного питания и пиществых производств</p>
            </div>
            </div>

        </div>
        <div class="ourservices__blocks">
            <div class="ourservices__block_common">
                <div class="otherservices__icon_block">
                    <div class="ourservices__icon">
                    <img src="wp-content/uploads/2017/03/laboratory.png">
                </div>
                </div>
                <div class="otherservices__block_text">
                    <h3 class="otherservices__block_header">Дезинфекция</h3>
                    <p>Наша компания предоставляет полный цикл услуг уничтожения бактерий, насекомых, грызунов и дезинфекции помещения. Мы работаем со всеми видами заказчиков, как домохозяйствами, так и предприятиями общественного питания и пиществых производств</p>
                </div>
            </div>
            <div class="ourservices__block_common">
                <div class="otherservices__icon_block">
                    <div class="ourservices__icon">
                        <img src="wp-content/uploads/2017/03/bee.png">
                    </div>
                </div>
                <div class="otherservices__block_text">
                    <h3 class="otherservices__block_header">Дезинсекция</h3>
                    <p>Наша компания предоставляет полный цикл услуг уничтожения бактерий, насекомых, грызунов и дезинфекции помещения. Мы работаем со всеми видами заказчиков, как домохозяйствами, так и предприятиями общественного питания и пиществых производств</p>
                </div>
            </div>
            <div class="ourservices__block_common">
                <div class="otherservices__icon_block">
                    <div class="ourservices__icon">
                        <img src="wp-content/uploads/2017/03/mouse.png">
                    </div>
                </div>
                <div class="otherservices__block_text">
                    <h3 class="otherservices__block_header">Дератизация</h3>
                    <p>Наша компания предоставляет полный цикл услуг уничтожения бактерий, насекомых, грызунов и дезинфекции помещения. Мы работаем со всеми видами заказчиков, как домохозяйствами, так и предприятиями общественного питания и пиществых производств</p>
                </div>
            </div>

        </div>
    </div>
<div class="mainbanner">
        <div class="portfolio-row-half">
            <div class="portfolio-grid-item-color">
                <div class="desc">
                    <h2>Мы эффективно уничтожим</h2>
                    <p class="grid__portfolio_text">Гарантированно уничножим Ваших врагов</p>
                    <p><a href="#" class="btn btn-primary btn-outline with-arrow">View All Projects <i class="icon-arrow-right22"></i></a></p>
                </div>
            </div>
            <a href="#" class="portfolio-grid-item" style="background-image: url(/wp-content/uploads/2017/03/viruswp.jpg);">
                <div class="desc2">
                    <h3>Дезинфекция</h3>
                    <span>Обезараживание от болезнетворных бактерий</span>
                    <i class="sl-icon-heart"></i>
                </div>
            </a>
            <a href="#" class="portfolio-grid-item" style="background-image: url(/wp-content/uploads/2017/03/mousewp.jpg);">
                <div class="desc2">
                    <h3>Дератизация</h3>
                    <span>Уничтожение грызунов</span>
                    <i class="sl-icon-heart"></i>
                </div>
            </a>
            <a href="#" class="portfolio-grid-item" style="background-image: url(/wp-content/uploads/2017/03/beewp.jpg);">
                <div class="desc2">
                    <h3>Дезинсекция</h3>
                    <span>Уничтожение насекомых</span>
                    <i class="sl-icon-heart"></i>
                </div>
            </a>
        </div>
    </div>
<div class="ourprices">
        <div class="ourprices__image">
            <img src="/wp-content/uploads/2017/03/accessory.png">
        </div>
        <div class="ourprices__head">
            <h3 class="ourprices__header">Наши цены</h3>
            <div class="ourprices__head_text">
                <div class="outprices__header_text">
                <p>Применение передовых технологий и препаратов позволяет уменьшить объем работы и поддерижвать разумную цену</p>
            </div>
            </div>
        </div>
        <div class="ourprices__blocks">
            <div class="ourprices__block_common">
                <h3 class="ourprices__common_head"><strong>Дезинфекция</strong></h3>
                <div class="outprices__common_currency">
                    <sup class="outprices__currency">&#8381;</sup>9999
                </div>
                <div class="outprices__common_desc">
                    <p>Обеззараживание поверхностей</p>
                </div>
                <hr class="outprices__divider">
                <ul class="outprices__common_list">
                    <li class="outprices__list_padding">Бактерии</li>
                    <li class="outprices__list_padding">Останки</li>
                    <li class="outprices__list_padding">Загрязнения</li>
                    <li class="outprices__list_padding">Уборка</li>
                </ul>
                <div class="ourprices__block_button">
                    <a class="btn btn-primary outprices__button_spec">Узнать больше</a>
                </div>
            </div>
            <div class="ourprices__block_common">
                <h3 class="ourprices__common_head"><strong>Дезинсекция</strong></h3>
                <div class="outprices__common_currency">
                    <sup class="outprices__currency">&#8381;</sup>5499
                </div>
                <div class="outprices__common_desc">
                    <p>Уничтожение насекомых</p>
                </div>
                <hr class="outprices__divider">
                <ul class="outprices__common_list">
                    <li class="outprices__list_padding">Тараканы</li>
                    <li class="outprices__list_padding">Клопы</li>
                    <li class="outprices__list_padding">Осы и пчелы</li>
                    <li class="outprices__list_padding">Клещи</li>
                </ul>
                <div class="ourprices__block_button">
                    <a class="btn btn-primary outprices__button_spec">Узнать больше</a>
                </div>
            </div>
            <div class="ourprices__block_common">
                <div class="salebox">
                    <div class="outprices__sale">
                        <div class="outprices__sale_box">
                            <div>
                                <p class="outprices__box_textcolor">Выгодно</p>
                            </div>
                        </div>
                    </div>
                    <div class="outprices__sale_triangle">
                        <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </div>
                </div>
                <div class="ourprices__block_sale"">
                    <h3 class="ourprices__common_head"><strong>Дератизация</strong></h3>
                    <div class="outprices__common_currency">
                        <sup class="outprices__currency">&#8381;</sup>19999
                    </div>
                    <div class="outprices__common_desc">
                        <p>Уничтожение грызунов</p>
                    </div>
                    <hr class="outprices__divider">
                    <ul class="outprices__common_list">
                        <li class="outprices__list_padding">Крысы</li>
                        <li class="outprices__list_padding">Мыши</li>
                        <li class="outprices__list_padding">Кроты</li>
                        <li class="outprices__list_padding">Прочие грызуны</li>
                    </ul>
                    <div class="ourprices__block_button">
                        <a class="btn btn-primary outprices__button_spec">Узнать больше</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="clientreview">
    <div class="clientreview__blocks">
        <div class="clientreview__blocks_common">
            <div class="otherservices__icon_block ">
                <div class="ourservices__icon clientreview__icon_block">
                    <img src="wp-content/uploads/2017/03/chat.png">
                </div>
            </div>
            <div class="clientreview__text">
                <p>Королева побагровела от ярости; несколько секунд она, не в силах выговорить ни слова, только бросала на Алису испепеляющие взгляды, а потом завизжала во все горло.</p>
            </div>
            <div class="client__review_desc">
                <p>Анатолий Сопрано старший</p>
            </div>
        </div>
        <div class="clientreview__blocks_common">
            <div class="otherservices__icon_block">
                <div class="ourservices__icon clientreview__icon_block">
                    <img src="wp-content/uploads/2017/03/chat.png">
                </div>
            </div>
            <div class="clientreview__text">
                <p>Алисе труднее всего давалось искусство управляться со своим фламинго; ей, правда, удалось довольно удобно взять птицу под мышку, так, чтобы длинные ноги не мешали.</p>
            </div>
            <div class="client__review_desc">
                <p>Полина Галтиери</p>
            </div>
        </div>
    </div>
</div>
