<?php

    //Bootstrap 3.3.7 CDN
    function bootstrap_enqueue_styles() {
        wp_enqueue_style( 'bootstrap-outlooker-style', get_stylesheet_uri());
        wp_register_style('bootstrap-style', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
        wp_enqueue_style( 'bootstrap-style');
    }
    add_action('wp_enqueue_scripts', 'bootstrap_enqueue_styles');

    function bootstraptheme_enqueue_styles() {
        wp_enqueue_style( 'bootstraptheme-outlooker-style', get_stylesheet_uri());
        wp_register_style('bootstraptheme-style', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css');
        wp_enqueue_style( 'bootstraptheme-style');
    }
    add_action('wp_enqueue_scripts', 'bootstrap_enqueue_styles');

    //Swiper Slider

    function swiper_enqueue_styles() {
        wp_enqueue_style( 'swiper-blattam-style');
        wp_register_style('swiper-style',  get_template_directory_uri() . '/template/css/swiper.css');
        wp_enqueue_style( 'swiper-style');
    }
    add_action('wp_enqueue_scripts', 'swiper_enqueue_styles');

    //Style CSS

    function desktop_enqueue_styles() {
        wp_enqueue_style( 'desktop-blattam-style');
        wp_register_style('desktop-style',  get_template_directory_uri() . '/template/css/desktop.css');
        wp_enqueue_style( 'desktop-style');
    }
    add_action('wp_enqueue_scripts', 'desktop_enqueue_styles');

    function smalldesktop_enqueue_styles() {
        wp_enqueue_style( 'smalldesktop-blattam-style');
        wp_register_style('smalldesktop-style',  get_template_directory_uri() . '/template/css/smalldesktop.css');
        wp_enqueue_style( 'smalldesktop-style');
    }
    add_action('wp_enqueue_scripts', 'smalldesktop_enqueue_styles');


    function pad_enqueue_styles() {
        wp_enqueue_style( 'pad-blattam-style');
        wp_register_style('pad-style',  get_template_directory_uri() . '/template/css/pad.css');
        wp_enqueue_style( 'pad-style');
    }
    add_action('wp_enqueue_scripts', 'pad_enqueue_styles');
    
    function phone_enqueue_styles() {
        wp_enqueue_style( 'phone-blattam-style');
        wp_register_style('phone-style',  get_template_directory_uri() . '/template/css/phone.css');
        wp_enqueue_style( 'phone-style');
    }
    add_action('wp_enqueue_scripts', 'phone_enqueue_styles');



    //Scripts
    function jquery1124_enqueue_scripts () {
        wp_register_script('jquery1124-outlooker','https://code.jquery.com/jquery-3.1.1.min.js');
        wp_enqueue_script('jquery1124-outlooker');
    }
    add_action('wp_enqueue_scripts', 'jquery1124_enqueue_scripts');


    function bootstrap_enqueue_scripts () {
        wp_register_script('bootstrap-outlooker','https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js');
        wp_enqueue_script('bootstrap-outlooker');
    }
    add_action('wp_enqueue_scripts', 'bootstrap_enqueue_scripts');

    //Slider Scripts

    function swiper_enqueue_scripts () {
        wp_register_script('script-swiper-blattam', get_template_directory_uri() . '/template/js/swiper.js');
        wp_enqueue_script('script-swiper-blattam');
    }
    add_action('wp_enqueue_scripts', 'swiper_enqueue_scripts');

    function customSlider_enqueue_scripts () {
        wp_register_script('customSlider-swiper-blattam', get_template_directory_uri() . '/template/js/customSlider.js');
        wp_enqueue_script('customSlider-swiper-blattam');
    }
    add_action('wp_enqueue_scripts', 'customSlider_enqueue_scripts');


    //CounterScript
    function jquerycountTo_enqueue_scripts () {
        wp_register_script('jquerycountTo-count-blattam', get_template_directory_uri() . '/template/js/jquery.countTo.js');
        wp_enqueue_script('jquerycountTo-count-blattam');
    }
    add_action('wp_enqueue_scripts', 'jquerycountTo_enqueue_scripts');

    function waypoints_enqueue_scripts () {
        wp_register_script('waypoints-count-blattam', get_template_directory_uri() . '/template/js/jquery.waypoints.min.js');
        wp_enqueue_script('waypoints-count-blattam');
    }
    add_action('wp_enqueue_scripts', 'waypoints_enqueue_scripts');

    function main_enqueue_scripts () {
        wp_register_script('main-count-blattam', get_template_directory_uri() . '/template/js/main.js');
        wp_enqueue_script('main-count-blattam');
    }
    add_action('wp_enqueue_scripts', 'main_enqueue_scripts');



    // Remove Wordpress auto <p> styling
    remove_filter( 'the_content', 'wpautop' );
    remove_filter( 'the_excerpt', 'wpautop' );


    ////Remove JS Contact Form 7 on each page
    add_filter( 'wpcf7_load_js', '__return_false' );
    //add_filter( 'wpcf7_load_css', '__return_false' );

    //Add Post Picture
    add_theme_support( 'post-thumbnails' );



?>
