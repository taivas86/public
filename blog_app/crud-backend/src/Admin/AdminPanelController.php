<?php

namespace App\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminPanelController extends AbstractController
{
    function showReactApp() {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        return $this->render('react-login-app/reactapp.html.twig');
    }
}