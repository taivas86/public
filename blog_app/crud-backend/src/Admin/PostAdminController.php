<?php

namespace App\Admin;

use App\Entity\Post;
use App\Entity\PostImage;
use App\Entity\PostType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class PostAdminController extends AbstractController
{
    private $entityManager;
    private $serializer;

    public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
    }

    /**
     * @Route (
     *     "/itpflege-rest/blog/post",
     *      name="create-post",
     *      methods={"POST"}
     *     )
     */
    public function createPost(Request $request)
    {
        $decodedPost = $this->decodeRequestContent($request);

        $postType = new PostType();
        $postType->setPostTypeName($decodedPost['postType']);

        $post = new Post();
        $post->setTitle($decodedPost['title'])
            ->setSlug($decodedPost['slug'])
            ->setNowAsPublishedAt()
            ->setContent($decodedPost['content'])
            ->setSeoTitle($decodedPost['seoTitle'])
            ->setSeoDesc($decodedPost['seoDesc'])
            ->setGoogleJson($decodedPost['googleJson'])
            ->setPostType($postType);

        $postImages = new PostImage();
        $postImages->setPreviewImagePath($decodedPost['previewImgPath'])
            ->setFullsizeImagePath($decodedPost['previewImgPath']);
        $postImages->setPost($post);

        $this->entityManager->persist($post);
        $this->entityManager->persist($postType);
        $this->entityManager->persist($postImages);

        $this->entityManager->flush();

        return new JsonResponse(
            [
                $this->serializer->serialize($post, 'json'),
            ],
            JsonResponse::HTTP_CREATED
        );
    }

    private function decodeRequestContent(Request $request)
    {
        return json_decode($request->getContent(), true);
    }

    /**
     * @Route (
     *     "/itpflege-rest/blog/",
     *     name="get-post-collection",
     *     methods={"GET"}
     *     )
     */
    public function getPosts()
    {
        $repository = $this->entityManager->getRepository(Post::class);
        $posts = $repository->findBy([], ['id' => 'DESC']);
        return JsonResponse::fromJsonString($this->serializer->serialize(
            $posts,
            'json'
            , ['groups' => 'post_list']));
    }

    /**
     * @Route (
     *     "/itpflege-rest/blog/post/{id}",
     *      name="get-post",
     *      methods={"GET"},
     *      requirements={"id"="\d+"}
     * )
     */
    public function getPost($id)
    {

        return JsonResponse::fromJsonString($this->serializer->serialize($this->findPostById($id),
            'json',
            ['groups' => 'post_list'])
        );
    }

    private function findPostById($id)
    {
        $repository = $this->entityManager->getRepository(Post::class);
        $post = $repository->findOneBy(['id' => $id]);

        if (!$post) {
            throw new NotFoundHttpException("No post with {$id} found");
        }

        $post->getPreviewPathFront();
        $post->getFullSizePathFront();
        return $post;
    }

    /**
     * @Route(
     *     "/itpflege-rest/blog/post/{id}",
     *     name="delete-post",
     *     methods={"DELETE"},
     *     requirements={"id"="\d+"}
     * )
     */
    public function deletePost($id)
    {
        $post = $this->findPostById($id);

        $this->entityManager->remove($post);
        $this->entityManager->flush();

        return new JsonResponse("", JsonResponse::HTTP_NO_CONTENT);
    }

    /**
     * @Route(
     *     "/itpflege-rest/blog/post/{id}",
     *     name="update-post",
     *     methods={"PUT"},
     *     requirements={"id"="\d+"}
     * )
     */
    public function updatePost($id, Request $request)
    {
        $post = $this->findPostById($id);

        $updatedData = $this->decodeRequestContent($request);

        $post->setTitle($updatedData['title'])
            ->setSlug($updatedData['slug'])
            ->setContent($updatedData['content'])
            ->setSeoTitle($updatedData['seoTitle'])
            ->setSeoDesc($updatedData['seoDesc'])
            ->setGoogleJson($updatedData['googleJson']);

        $postImages = new PostImage();
        $postImages->setPreviewImagePath($updatedData['previewPathFront'])
            ->setFullsizeImagePath($updatedData['fullSizePathFront']);
        $postImages->setPost($post);

        $this->entityManager->persist($post);
        $this->entityManager->persist($postImages);

        $this->entityManager->flush();

        return new JsonResponse(
            [
                $this->serializer->serialize(
                    $post,
                    'json',
                    ['groups' => 'post_list']),
            ],
            JsonResponse::HTTP_OK
        );
    }
}