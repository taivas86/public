<?php

namespace App\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Post;


class SitemapController extends AbstractController
{
    /**
     * @Route ("/sitemap.xml", name="sitemap")
     */

    public function createSitemap(Request $request, EntityManagerInterface $em)
    {
        $hostname = $request->getSchemeAndHttpHost();
        $links = [];
        //Add static
        $links[] = ['loc' => $this->generateUrl('home-page')];
        $links[] = ['loc' => $this->generateUrl('how-we-work')];
        $links[] = ['loc' => $this->generateUrl('services-list')];
        $links[] = ['loc' => $this->generateUrl('works')];
        $links[] = ['loc' => $this->generateUrl('blog')];
        $links[] = ['loc' => $this->generateUrl('contacts')];

        //add dynamically routes
        $posts = $em->getRepository(Post::class)->findAll();

        foreach ($posts as $post) {

            if ($post->getPostType() == 'post') {
                $links[] = [
                    'loc' => $this->formatSlug($this->generateUrl('blog', ['slug' => $post->getSlug()])),
                ];
            }

            if ($post->getPostType() == 'product') {
                $links[] = [
                    'loc' => $this->formatSlug($this->generateUrl('services-list', ['slug' => $post->getSlug()])),
                ];
            }
        }

        $count = count($links);
        for ($i = 0; $i < $count; $i++) {
            $links[$i]['lastmod'] = $post->getPublishedAt();
            $links[$i]['changefreq'] = "weekly";
            $links[$i]['priority'] = 0.8;
        }

        $response = new Response(
            $this->renderView('sitemap.html.twig', [
                'urls' => $links,
                'hostname' => $hostname
            ]),
            200
        );

        $response->headers->set('Content-Type', 'text/xml');
        return $response;

    }

    private function formatSlug(string $url): string
    {
        return str_replace("slug=", "", str_replace('?', '/', $url));
    }

}
