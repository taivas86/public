<?php

namespace App\Client;

use App\Entity\Post;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    function getPostData()
    {
        $posts = $this->entityManager->getRepository(Post::class);

        if (!$posts) {
            throw $this->createNotFoundException('No post. Check DB connection or create new');
        }

        return $posts;
    }

    /**
     * @Route ("/", name="home-page")
     */
    function showIndexPage()
    {
        $pageTemplate = $this->getPostData()->findOneBy(array('title' => 'front page'));

        $term = 'post';
        $threeLatestPosts = $this->getPostData()->getLatestPostsFrontpage($term);

        return $this->render('client-side/front-page.html.twig', [
            'frontpage' => $pageTemplate,
            'frontpagePosts' => $threeLatestPosts
        ]);
    }

    /**
     * @Route ("/customer-services", name="how-we-work")
     */
    function showCustomerServices()
    {
        $pageTemplate = $this->getPostData()->findOneBy(array('title' => 'how we work'));

        return $this->render('client-side/customer-services.html.twig', [
            'customerServices' => $pageTemplate,
        ]);
    }

    /**
     * @Route ("/services", name="services-list")
     */
    function showServiceList()
    {
        $pageTemplate = $this->getPostData()->findOneBy(array('title' => 'front page'));

        return $this->render('client-side/services-list.html.twig', [
            'serviceslist' => $pageTemplate,
        ]);
    }

    /**
     * @Route ("/services/{slug}", name="show_services_item")
     */
    public function showSingleService($slug)
    {
        $post = $this->getPostData()->findOneBy(['slug' => $slug]);
        $postType = $post->getPostType();
        if (!$post || $postType !="product") {
            throw $this->createNotFoundException(sprintf('No post for url "%s"', $slug));
        }

        return $this->render('client-side/post.html.twig', [
            'post' => $post,
        ]);
    }

    /**
     * @Route ("/works", name="works")
     */
    function showPortfolio()
    {
        $pageTemplate = $this->getPostData()->findOneBy(array('title' => 'works'));

        return $this->render('client-side/portfolio.html.twig', [
            'works' => $pageTemplate,
        ]);
    }

    /**
     * @Route ("/blog", name="blog")
     */
    function showBlogPage()
    {
        $term = 'post';
        $posts = $this->getPostData()->getPostsBlog($term);

        return $this->render('client-side/blog.html.twig', [
            'posts' => $posts
        ]);
    }

    /**
     * @Route ("/blog/{slug}", name="show_blog_item")
     */
    public function showPost($slug)
    {
        $post = $this->getPostData()->findOneBy(['slug' => $slug]);

        $postType = $post->getPostType();

        if (!$post || $postType != 'post') {
            throw $this->createNotFoundException(sprintf('No post for url "%s"', $slug));
        }

        return $this->render('client-side/post.html.twig', [
            'post' => $post,
        ]);
    }

    /**
     * @Route ("/contacts", name="contacts")
     */
    function showContacts()
    {
        $pageTemplate = $this->getPostData()->findOneBy(array('title' => 'contacts'));

        return $this->render('client-side/contacts.html.twig', [
            'contacts' => $pageTemplate,
        ]);
    }

}
