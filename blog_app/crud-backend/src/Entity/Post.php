<?php

namespace App\Entity;

use App\Repository\PostRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=PostRepository::class)
 */
class Post
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"post_list"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"post_list"})
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=175, unique=true)
     * @Groups({"post_list"})
     */
    private $slug;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTime $publishedAt;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"post_list"})
     */
    private $content;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $seo_title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $seo_desc;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $google_json;

    /**
     * @ORM\ManyToOne(targetEntity=PostType::class, inversedBy="post")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"post_list"})
     */
    private $postType;

    /**
     * @ORM\OneToMany(targetEntity=PostImage::class, mappedBy="post", cascade={"remove"})
     */
    private $postImages;

    /**
     * @Groups({"post_list"})
     */
    private $previewPath;

    /**
     * @Groups({"post_list"})
     */
    private $fullSizePath;


    public function __construct()
    {
        $this->postImages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @Groups({"post_list"})
     */
    public function getPublishedAt()
    {
        return $this->publishedAt->format('c');
    }

    public function setPublishedAt(DateTimeInterface $publishedAt): self
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @Groups({"post_list"})
     */
    public function getSeoTitle(): ?string
    {
        return $this->seo_title;
    }

    public function setSeoTitle(?string $seo_title): self
    {
        $this->seo_title = $seo_title;

        return $this;
    }

    /**
     * @Groups({"post_list"})
     */
    public function getSeoDesc(): ?string
    {
        return $this->seo_desc;
    }

    public function setSeoDesc(?string $seo_desc): self
    {
        $this->seo_desc = $seo_desc;

        return $this;
    }

    /**
     * @Groups({"post_list"})
     */
    public function getGoogleJson(): ?string
    {
        return $this->google_json;
    }

    public function setGoogleJson(?string $google_json): self
    {
        $this->google_json = $google_json;

        return $this;
    }

    public function setNowAsPublishedAt(): self
    {
        $this->publishedAt = new DateTime;

        return $this;
    }

    /*PostType get & set*/
    /**
     * @Groups({"post_list"})
     */
    public function getPostType()
    {
        return $this->postType->getPostTypeName();
    }

    public function setPostType(?PostType $postType): self
    {
        $this->postType = $postType;
        return $this;
    }

    /**
     * @return Collection|PostImage[]
     */
    public function getPostImages()
    {
        return $this->postImages;
    }

    public function addPostImage(PostImage $postImage): self
    {
        if (!$this->postImages->contains($postImage)) {
            $this->postImages[] = $postImage;
            $postImage->setPost($this);
        }

        return $this;
    }

    public function removePostImage(PostImage $postImage): self
    {
        if ($this->postImages->removeElement($postImage)) {
            // set the owning side to null (unless already changed)
            if ($postImage->getPost() === $this) {
                $postImage->setPost(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     * @Groups({"post_list"})
     */
    public function getPreviewPathFront()
    {
        $arr = $this->postImages->toArray();
        foreach ($arr as $value) {
            $this->previewPath = $value->getPreviewImagePath();
        }
        return $this->previewPath;
    }

    /**
     * @return mixed
     * @Groups({"post_list"})
     */
    public function getFullSizePathFront()
    {
        $arr = $this->postImages->toArray();
        foreach ($arr as $value) {
            $this->fullSizePath = $value->getFullsizeImagePath();
        }
        return $this->fullSizePath;
    }



}
