<?php

namespace App\Entity;

use App\Repository\PostImageRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PostImageRepository::class)
 */
class PostImage
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $previewImagePath;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fullsizeImagePath;

    /**
     * @ORM\ManyToOne(targetEntity=Post::class, inversedBy="postImages")
     */
    private $post;

    // https://stackoverflow.com/questions/55948523/symfony-4-object-of-class-could-not-be-converted-to-string
    public function __toString()
    {
        return $this->previewImagePath;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPreviewImagePath(): ?string
    {
        return $this->previewImagePath;
    }

    public function setPreviewImagePath(string $previewImagePath): self
    {
        $this->previewImagePath = $previewImagePath;

        return $this;
    }

    public function getFullsizeImagePath(): ?string
    {
        return $this->fullsizeImagePath;
    }

    public function setFullsizeImagePath(string $fullsizeImagePath): self
    {
        $this->fullsizeImagePath = $fullsizeImagePath;

        return $this;
    }

    public function getPost(): ?Post
    {
        return $this->post;
    }

    public function setPost(?Post $post): self
    {
        $this->post = $post;

        return $this;
    }
}
