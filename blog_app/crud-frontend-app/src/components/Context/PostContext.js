import React, { useState, useEffect, createContext } from 'react';
import { fetchPosts } from '../Utils/FetchUtil';

export const PostContext = createContext();

export const PostProvider = (props) => {
  const [posts, setPosts] = useState([]);

  const getPosts = async () => {
    const receivedPosts = await fetchPosts();
    setPosts(receivedPosts);
  };

  useEffect(() => {
    getPosts();
  }, []);

  return (
    <PostContext.Provider value={
      [posts, setPosts]
      }
    >
      {props.children}
    </PostContext.Provider>
  );
};
