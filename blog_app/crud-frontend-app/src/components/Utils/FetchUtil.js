// Fetch Post Utility
export const fetchPosts = async () => {
  const result = await fetch('https://localhost:8000/itpflege-rest/blog');
  const data = await result.json();
  console.log(data);
  return data;
};
