import React, { useState } from 'react';
import PropTypes, { func } from 'prop-types';

const Post = ({ post, getPostData, selected }) => {
  // callback selected post object to Posts component and open editor state

  const selectPost = () => {
    getPostData(post);
    selected(true);
  };

  return (
    <a
      className="post__wrapper"
      onClick={() => {
        selectPost();
      }}
      aria-hidden="true"
    >
      {post.title}
    </a>
  );
};

// https://github.com/yannickcr/eslint-plugin-react/issues/2079
Post.propTypes = {
  post: PropTypes.instanceOf(Object).isRequired,
  getPostData: PropTypes.func.isRequired,
  selected: PropTypes.func.isRequired,
};

export default Post;
