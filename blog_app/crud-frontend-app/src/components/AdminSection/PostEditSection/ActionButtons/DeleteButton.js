import React from 'react';
import PropTypes from 'prop-types';

const DeleteButton = ({ deleteSelectedPost }) => (
  <input
    type="submit"
    name="deleteButton"
    value="Delete Post"
    className="uk-button uk-button-danger"
    onClick={deleteSelectedPost}
  />
);

DeleteButton.propTypes = {
  deleteSelectedPost: PropTypes.func.isRequired,
};

export default DeleteButton;
