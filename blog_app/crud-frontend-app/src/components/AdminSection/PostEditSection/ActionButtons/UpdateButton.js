import React from 'react';
import PropTypes from 'prop-types';

const UpdateButton = ({ handleUpdate }) => (
  <input
    type="submit"
    name="updateButton"
    value="Update Post"
    className="uk-button uk-button-secondary"
    onClick={handleUpdate}
  />
);

UpdateButton.propTypes = {
  handleUpdate: PropTypes.func.isRequired,
};

export default UpdateButton;
