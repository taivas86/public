import React from 'react';
import PropTypes from 'prop-types';

const CreatePostButton = ({ createNewPost }) => (
  <input
    type="submit"
    name="createPostButton"
    value="Create new post"
    className="uk-button uk-button-primary"
    onClick={createNewPost}
  />
);

CreatePostButton.propTypes = {
  createNewPost: PropTypes.func.isRequired,
};

export default CreatePostButton;
