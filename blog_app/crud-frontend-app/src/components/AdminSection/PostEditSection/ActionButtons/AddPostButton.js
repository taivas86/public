import React from 'react';
import PropTypes from 'prop-types';

const AddPostButton = ({ addNewPost, newPostEvent }) => (
  <div className="uk-navbar-item">
    <button className="uk-button uk-button-default" onClick={addNewPost} type="button">
      { newPostEvent ? 'Exit from creation mode' : 'Create new post'}
    </button>
  </div>
);

AddPostButton.propTypes = {
  newPostEvent: PropTypes.bool.isRequired,
  addNewPost: PropTypes.func.isRequired,
};

export default AddPostButton;
