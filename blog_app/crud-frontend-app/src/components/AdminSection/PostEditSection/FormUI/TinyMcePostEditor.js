import React from 'react';
import PropTypes from 'prop-types';

import 'tinymce/tinymce';
import 'tinymce/icons/default';
import 'tinymce/themes/silver';
import 'tinymce/plugins/paste';
import 'tinymce/plugins/link';
import 'tinymce/plugins/image';
import 'tinymce/plugins/table';
import 'tinymce/plugins/code';
import 'tinymce/skins/ui/oxide/skin.min.css';
import 'tinymce/skins/ui/oxide/content.min.css';
import 'tinymce/skins/content/default/content.min.css';
import { Editor } from '@tinymce/tinymce-react';

const PostEditor = ({ content, getUpdatedText }) => {
  const handleEditorChange = (e) => {
    getUpdatedText(e);
  };

  return (
    <Editor
      initialValue={content}
      init={{
        skin: false,
        content_css: false,
        height: 500,
        menubar: true,
        plugins: [
          'link image',
          'table paste',
          'code',
        ],
        toolbar:
        'code | undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
        selector: 'textarea',
        extended_valid_elements: '*[*]',
        valid_elements: '*[*]',

      }}
      onEditorChange={handleEditorChange}
    />
  );
};

PostEditor.propTypes = {
  content: PropTypes.string.isRequired,
  getUpdatedText: PropTypes.func.isRequired,
};

export default PostEditor;
