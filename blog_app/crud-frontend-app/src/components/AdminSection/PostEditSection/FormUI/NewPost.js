import React, { useState, useContext } from 'react';
import PostEditor from './TinyMcePostEditor';
import CreatePostButton from '../ActionButtons/CreatePostButton';
import { PostContext } from '../../../Context/PostContext';
import { fetchPosts } from '../../../Utils/FetchUtil';

const NewPost = () => {
  const [posts, setPosts] = useContext(PostContext);
  const [postType, setPostType] = useState();
  const [title, setTitle] = useState('');
  const [slug, setSlug] = useState('');
  const [content, setContent] = useState(null);
  const [previewImgPath, setPreviewImgPath] = useState('');
  const [fullImgPath, setFullImgPath] = useState('');
  const [seoTitle, setSeoTitle] = useState('');
  const [seoDesc, setSeoDesc] = useState('');
  const [googleJson, setGoogleJson] = useState('');

  const handlePostTypeChange = (event) => {
    const value = event.target.getAttribute('value');
    setPostType(value);
  };

  const getUpdatedText = (newContent) => {
    setContent(newContent);
  };

  const newPostObject = {
    postType, title, slug, content, previewImgPath, fullImgPath, seoTitle, seoDesc, googleJson,
  };

  const getPosts = async () => {
    const receivedPosts = await fetchPosts();
    setPosts(receivedPosts);
  };

  // https://stackoverflow.com/questions/52780706/tinymce-react-clear-editor-content
  const [editorKey, setEditorKey] = useState(4);
  const clearEditor = () => {
    const newKey = editorKey * 43;
    setEditorKey(newKey);
  };

  const clearPosts = () => {
    setPostType();
    setTitle('');
    setSlug('');
    setSeoTitle('');
    setSeoDesc('');
    setGoogleJson('');
    setPreviewImgPath('');
    setFullImgPath('');
  };

  const createNewPost = async () => {
    await fetch('https://localhost:8000/itpflege-rest/blog/post',
      {
        method: 'POST',
        headers: {
          'Content-type': 'application/json',
        },
        body: JSON.stringify(newPostObject),
      });
    getPosts();
    clearPosts();
    clearEditor();
  };

  return (
    <>
      <form>
        <div className="form__control">
          <p>Choose content type</p>
          <div className="uk-margin uk-grid-small uk-child-width-auto uk-grid">
            <label htmlFor="postType">
              <input
                id="windows"
                className="uk-radio"
                value="post"
                name="postType"
                type="radio"
                onChange={handlePostTypeChange}
              />
              Post
            </label>

            <label htmlFor="postType">
              <input
                id="mac"
                className="uk-radio"
                value="page"
                name="postType"
                type="radio"
                onChange={handlePostTypeChange}
              />
              Page
            </label>

            <label htmlFor="postType">
              <input
                id="linux"
                className="uk-radio"
                value="portfolio"
                name="postType"
                type="radio"
                onChange={handlePostTypeChange}
              />
              Portfolio
            </label>

            <label htmlFor="postType">
              <input
                id="linux"
                className="uk-radio"
                value="product"
                name="postType"
                type="radio"
                onChange={handlePostTypeChange}
              />
              Product
            </label>

          </div>
        </div>

        <div className="form__control">
          <label className="post__field" htmlFor="postTitle">
            <span className="post__label">Post Title:</span>
            <input
              className="post__input"
              type="text"
              value={title}
              onChange={(event) => {
                setTitle(event.target.value);
              }}
            />
          </label>
        </div>
        <div className="form__control">
          <label className="post__field" htmlFor="postSlug">
            <span className="post__label">Post Slug:</span>
            <input
              className="post__input"
              type="text"
              value={slug}
              required="required"
              onChange={(event) => { setSlug(event.target.value); }}
            />
          </label>
        </div>
        <div className="form__control">
          <PostEditor key={editorKey} getUpdatedText={getUpdatedText} />
        </div>
        <div className="form__control">
          <label className="post__field" htmlFor="previewImgPath">
            <span className="post__label">Preview Img Path:</span>
            <input
              className="post__input"
              type="text"
              value={previewImgPath}
              onChange={(event) => { setPreviewImgPath(event.target.value); }}
            />
          </label>
        </div>
        <div className="form__control">
          <label className="post__field" htmlFor="fullImgPath">
            <span className="post__label">Full Img Path:</span>
            <input
              className="post__input"
              type="text"
              value={fullImgPath}
              onChange={(event) => { setFullImgPath(event.target.value); }}
            />
          </label>
        </div>
        <div className="form__control">
          <label className="post__field" htmlFor="seoTitle">
            <span className="post__label">SEO Title:</span>
            <input
              className="post__input"
              type="text"
              value={seoTitle}
              onChange={(event) => { setSeoTitle(event.target.value); }}
            />
          </label>
        </div>
        <div className="form__control">
          <label className="post__field" htmlFor="seoDescrioption">
            <span className="post__label">SEO Desc:</span>
            <input
              className="post__input"
              type="text"
              value={seoDesc}
              onChange={(event) => { setSeoDesc(event.target.value); }}
            />
          </label>
        </div>
        <div className="form__control">
          <label htmlFor="postTitle">
            <span className="post__label">
              <p>
                Google JSON-LD Tech Tag: (
                <a target="_blank" href="https://technicalseo.com/tools/schema-markup-generator/" rel="noreferrer">Creating tool</a>
                )
              </p>
            </span>
            <textarea
              className="post__input post__textarea_font post__input_height"
              type="text"
              value={googleJson}
              onChange={(event) => { setGoogleJson(event.target.value); }}
            />
          </label>
        </div>
      </form>
      <div className="posteditor__buttonblock">
        <CreatePostButton createNewPost={createNewPost} />
      </div>
    </>
  );
};

export default NewPost;
