import React, { useState, useEffect, useContext } from 'react';
import PropTypes from 'prop-types';
import UpdateButton from '../ActionButtons/UpdateButton';
import DeleteButton from '../ActionButtons/DeleteButton';
import Form from './Form';
import { PostContext } from '../../../Context/PostContext';
import { fetchPosts } from '../../../Utils/FetchUtil';

const PostForm = ({ post }) => {
  const [posts, setPosts] = useContext(PostContext);
  // get data form
  const [id, setId] = useState();
  const [title, setTitle] = useState('');
  const [slug, setSlug] = useState('');
  const [content, setContentEditor] = useState('');
  const [seoTitle, setSeoTitle] = useState('');
  const [seoDesc, setSeoDesc] = useState('');
  const [googleJson, setGoogleJson] = useState('');

  const getFormData = (fieldsFromForm) => {
    useEffect(() => {
      setId(post.id);
      setTitle(fieldsFromForm.title);
      setSlug(fieldsFromForm.slug);
      setContentEditor(fieldsFromForm.content);
      setSeoTitle(fieldsFromForm.seoTitle);
      setSeoDesc(fieldsFromForm.seoDesc);
      setGoogleJson(fieldsFromForm.googleJson);
    });
  };

  // Update Post Methods
  const getPosts = async () => {
    const receivedPosts = await fetchPosts();
    setPosts(receivedPosts);
  };

  // Delete Post Methods
  const deletePost = async () => {
    await fetch(`https://localhost:8000/itpflege-rest/blog/post/${id}`, { method: 'DELETE' });
    setPosts(posts.filter((element) => element.id !== id));
  };
  const deleteSelectedPost = (event) => {
    event.preventDefault();
    deletePost(id);
  };

  const updatePost = async (postForSend) => {
    await fetch(`https://localhost:8000/itpflege-rest/blog/post/${postForSend.id}`,
      {
        method: 'PUT',
        headers: {
          'Content-type': 'application/json',
        },
        body: JSON.stringify(postForSend),
      });
    getPosts();
	 alert("Post updated");
  };

  const handleUpdate = (event) => {
    event.preventDefault();
    updatePost({
      id, title, slug, content, seoTitle, seoDesc, googleJson,
    });
  };

  return (
    <>
      <Form post={post} getFormData={getFormData} />
      <div className="posteditor__buttonblock">
        <UpdateButton handleUpdate={handleUpdate} />
        <DeleteButton deleteSelectedPost={deleteSelectedPost} />
      </div>
    </>
  );
};

PostForm.propTypes = {
  post: PropTypes.instanceOf(Object).isRequired,
};

export default PostForm;
