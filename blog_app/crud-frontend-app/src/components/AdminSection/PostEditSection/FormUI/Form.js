import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import PostEditor from './TinyMcePostEditor';

const Form = ({ post, getFormData }) => {
  const [title, setTitle] = useState('');
  const [slug, setSlug] = useState('');
  const [content, setContent] = useState(post.content);
  const [seoTitle, setSeoTitle] = useState('');
  const [seoDesc, setSeoDesc] = useState('');
  const [googleJson, setGoogleJson] = useState('');

  useEffect(() => {
    setTitle(post.title);
    setSlug(post.slug);
    setSeoTitle(post.seoTitle);
    setSeoDesc(post.seoDesc);
    setGoogleJson(post.googleJson);
  }, [post]);

  const getUpdatedText = (newContent) => {
    setContent(newContent);
  };

  const sendUpdatedPost = () => {
    getFormData({
      title, slug, seoTitle, content, seoDesc, googleJson,
    });
  };

  return (
    <form onChange={sendUpdatedPost()}>
      <div className="form__control">
        <label className="post__field" htmlFor="postTitle">
          <span className="post__label">Post Title:</span>
          <input
            className="post__input"
            type="text"
            value={title}
            onChange={(event) => { setTitle(event.target.value); }}
          />
        </label>
      </div>
      <div className="form__control">
        <label className="post__field" htmlFor="postSlug">
          <span className="post__label">Post Slug:</span>
          <input
            className="post__input"
            type="text"
            value={slug}
            onChange={(event) => { setSlug(event.target.value); }}
          />
        </label>
      </div>
      <div id="textEditor" className="form__control">
        <PostEditor content={post.content} getUpdatedText={getUpdatedText} />
      </div>
      <div className="form__control">
        <label className="post__field" htmlFor="seoTitle">
          <span className="post__label">SEO Title:</span>
          <input
            className="post__input"
            type="text"
            value={seoTitle}
            onChange={(event) => { setSeoTitle(event.target.value); }}
          />
        </label>
      </div>
      <div className="form__control">
        <label className="post__field" htmlFor="seoDescrioption">
          <span className="post__label">SEO Desc:</span>
          <input
            className="post__input"
            type="text"
            value={seoDesc}
            onChange={(event) => { setSeoDesc(event.target.value); }}
          />
        </label>
      </div>
      <div className="form__control">
        <label htmlFor="postTitle">
          <span className="post__label">
            <p>
              Google JSON-LD Tech Tag: (
              <a target="_blank" href="https://technicalseo.com/tools/schema-markup-generator/" rel="noreferrer">Creating tool</a>
              )
            </p>
          </span>
          <textarea
            className="post__input post__textarea_font post__input_height"
            type="text"
            value={googleJson}
            onChange={(event) => { setGoogleJson(event.target.value); }}
          />
        </label>
      </div>
    </form>
  );
};

Form.propTypes = {
  post: PropTypes.instanceOf(Object).isRequired,
  getFormData: PropTypes.func.isRequired,
};

export default Form;
