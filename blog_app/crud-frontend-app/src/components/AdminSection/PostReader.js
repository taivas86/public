import React, { useState, useContext } from 'react';
import './AdminSection.scss';
import PostListElement from './Post';
import PostForm from './PostEditSection/FormUI/PostFormBody';
import NewPost from './PostEditSection/FormUI/NewPost';
import AddPostButton from './PostEditSection/ActionButtons/AddPostButton';
import { PostContext } from '../Context/PostContext';

const Posts = () => {
  const [posts] = useContext(PostContext);
  // data from selected post
  const [data, setData] = useState([]);

  // set store data from selected post
  const getPostData = (post) => {
    setData(post);
  };

  const [openEditor, setOpenEditorOpen] = useState(false);
  const isEditorOpen = (state) => {
    setOpenEditorOpen(state);
  };

  const [newPostEvent, setNewPostEvent] = useState(false);
  const addNewPost = () => {
    setNewPostEvent(!newPostEvent);
  };

  // If db empty or not connection
  if (posts.length === 0 && newPostEvent === false) {
    return (
      <div>
        <h4>No posts in DB. Checks connection or create new post for database init</h4>
        <NewPost />
      </div>
    );
  }

  // post filtering
  const postItems = posts.filter((post) => post.postType === 'post');
  const portfolioItems = posts.filter((post) => post.postType === 'portfolio');
  const pageItems = posts.filter((post) => post.postType === 'page');
  const productItems = posts.filter((post) => post.postType === 'product');

  return (
    <div className="posts">
      <div className="posts__header">
        <div className="uk-text-lead post__add ">
          <div>
            <h3>Posts: </h3>
          </div>
          {/* Кнопка добавления нового поста, устанавливающая эвент создания в true */}
          <AddPostButton addNewPost={addNewPost} newPostEvent={newPostEvent} />
        </div>
      </div>
      <div className="post_panel">
        <div className="post__panel_left">

          <details>
            <summary className="post__accordition">Posts</summary>
            {postItems.map((post) => (
              <PostListElement
                key={post.id}
                post={post}
                getPostData={getPostData}
                selected={isEditorOpen}
              />
            ))}
          </details>

          <details>
            <summary className="portfolio__accordition">Portfolio</summary>
            {portfolioItems.map((post) => (
              <PostListElement
                key={post.id}
                post={post}
                getPostData={getPostData}
                selected={isEditorOpen}
              />
            ))}
          </details>

          <details>
            <summary className="products__accordition">Products</summary>
            {productItems.map((post) => (
              <PostListElement
                key={post.id}
                post={post}
                getPostData={getPostData}
                selected={isEditorOpen}
              />
            ))}
          </details>

          <details>
            <summary className="pages__accordition">Page Templates</summary>
            {pageItems.map((post) => (
              <PostListElement
                key={post.id}
                post={post}
                getPostData={getPostData}
                selected={isEditorOpen}
              />
            ))}
          </details>

        </div>

        <div className="post__panel_right">
          {openEditor === true && newPostEvent === false
            ? (
              <PostForm
                post={data}
              />
            )
            : (<h4 className={` ${newPostEvent && 'posteditor__notify_disable'}`}>Select post or create new</h4>)}

          {/* Если эвент нажатой кнопки поста true -> показать форму компонента NewPost */}
          {newPostEvent && <NewPost />}
        </div>
      </div>
    </div>
  );
};

export default Posts;
