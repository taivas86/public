import React from 'react';
import './assets/App.scss';
import './assets/AppParent.scss';
import PostReader from './components/AdminSection/PostReader';
import { PostProvider } from './components/Context/PostContext';

function App() {
  return (
    <PostProvider>
      <div className="uk-container uk-container-expand app__container">
        <div className="post__panel">
          <PostReader />
        </div>
      </div>
    </PostProvider>
  );
}
export default App;
