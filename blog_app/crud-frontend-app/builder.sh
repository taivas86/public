#!/bin/bash
yarn build

#js assets
cp -vf build/static/js/main.*.js    ../backend/public/bundles/AdminReactApp/appbuild/main.js
cp -vf build/static/js/[2]*.*.chunk.js  ../backend/public/bundles/AdminReactApp/appbuild/main2.js
cp -vf build/static/js/[3]*.*.chunk.js  ../backend/public/bundles/AdminReactApp/appbuild/main3.js

#css assets
cp -vf build/static/css/main.*.css  ../backend/public/bundles/AdminReactApp/appbuild/main.css
cp -vf build/static/css/[0-9]*.*.chunk.css  ../backend/public/bundles/AdminReactApp/appbuild/main.chunk.css

#index.hmtl
cp -vf build/index.html ../backend/public/bundles/AdminReactApp/appbuild/index.html

