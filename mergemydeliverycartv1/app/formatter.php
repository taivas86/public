<?php
//catMyGroceryCart

$jsonRaw = $_POST['bill'];
$invoice = get_object_vars(json_decode($jsonRaw));
$tableData = [];

$date = new DateTime();
$date = $date->setTimestamp($invoice['dateTime']);
$buyDate = $date->format('d-m-Y h:m:s');
$tableData['buyDate'] = $buyDate;

$seller = $invoice['user'];
$tableData['seller'] = $seller;

$totalAmount = $invoice['totalSum'] / 100;
$tableData['totalAmount'] = $totalAmount;

$itemsArray = $invoice['items'];
$len = count($itemsArray);
for ($i = 0; $i < $len; $i++) {
    $entry = get_object_vars($itemsArray[$i]);
    $itemsArray[$i] = ['entryName' => $entry['name'],
                    'entryQuantify' => $entry['quantity'],
                    'entryPricePerOne' => $entry['price'] / 100,
                    'entryTotalAmount' => $entry['sum'] / 100
    ];
}

$tableData['items'] = $itemsArray;

$catList = array_map('str_getcsv', file($_SERVER['DOCUMENT_ROOT'] . '/assets/categories.csv'));

//Payload
$output = ['invoice' => $tableData, 'accountCats' => $catList];
$payload['success'] = false;
if ($_POST['bill']) {
    $payload['success'] = true;
    $payload['output'] = $output;
}
echo json_encode($payload);

