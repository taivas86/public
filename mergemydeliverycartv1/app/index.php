<html>
<head>
    <meta charset="utf-8"/>
    <title>Категорирование тележки</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.15.22/dist/css/uikit.min.css"/>
    <link rel="stylesheet" href="assets/css/main.css" ;
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.15.22/dist/js/uikit.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.15.22/dist/js/uikit-icons.min.js"></script>
    <script src="assets/js/uploadFile.js"></script>
    <script src="assets/js/calculate.js"></script>
    <script src="assets/js/resetPage.js"></script>
</head>
<body>
<div class="upload">
    <h3 class="upload__title">Выберите JSON для загрузки</h3>
    <form>
        <input id="fileupload" type="file" name="fileupload"/>
        <button type="button" class="uk-button uk-button-primary upload__button" id="upload-button"
                onclick="uploadJson()">Загрузить
        </button>
        <button type="reset" class="uk-button uk-button-danger" onclick="resetPage()">Сбросить</button>
    </form>
</div>
<div class="description">
    <div id="descriptionInfo">
        <div class="table__block">
            <p class="first">Дата покупки: </p>
            <p id="date"></p>
        </div>
        <div class="table__block">
            <p class="first">Магазин: </p>
            <p id="seller"></p>
        </div>
        <div class="table__block">
            <p class="first">Общая стоимость по чеку: </p>
            <p id="totalAmount"></p>
        </div>
    </div>

    <div id="descriptionTable">
        <h3 id="descriptionHeader">Таблица выбора категорий</h3>
    </div>

    <input id="mergeCats"
           type="button"
           class="uk-button uk-button-secondary merge__button"
           value="Объединить в категории"
           onclick="calculate()"
    />

</div>
<div id="result">
    <h3>Реконсиляция чека</h3>
    <div id="reconciliation">
    </div>
    <h3>Категории</h3>
    <div id="resultTable"></div>
</div>
</body>
</html>

