async function uploadJson() {
  // upload file
  const file = document.getElementById('fileupload').files[0];
  const fr = new FileReader();
  // onload async
  fr.onload = preview;
  fr.readAsText(file);

  // prepare invoice and fetch api response to backend
  async function preview(e) {
    const lines = e.target.result;
    const newArr = JSON.parse(lines);
    const strings = JSON.stringify(newArr);

    const response = await fetch('formatter.php', {
      method: 'POST',
      Accept: 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded',
      body: new URLSearchParams({ bill: strings }),
    });
    const { success, output } = await response.json();
    if (success) {
      createBillView(output);
    }
  }

  // create table
  function createBillView(output) {
    const { invoice } = output;
    const invoiceBlock = document.getElementById('descriptionTable');
    document.getElementById('descriptionInfo').style.display = 'block';
    document.getElementById('date').innerText = invoice.buyDate;
    document.getElementById('seller').innerText = invoice.seller;
    document.getElementById('totalAmount').innerText = `${invoice.totalAmount} рублей`;

    const categories = output.accountCats.sort();
    const catsLen = categories.length;

    // catslist select
    let selectString = '';
    selectString += '<option>Выберите категорию</option>';
    for (let i = 0; i < catsLen; i += 1) {
      selectString += `<option>${categories[i]}</option>`;
    }

    // create table
    const { items } = invoice;
    const itemsLen = items.length;
    const table = document.createElement('table');
    table.setAttribute('class', 'uk-table uk-table-striped uk-table-small uk-table-responsive');
    table.setAttribute('id', 'selectTable');

    // fill table
    for (let i = 0; i < itemsLen; i += 1) {
      const row = table.insertRow(i);
      row.setAttribute('class', 'product__entry');
      const eltTable = items[i];
      row.insertCell(0).innerHTML = eltTable.entryName;
      row.insertCell(1).innerHTML = eltTable.entryQuantify;
      row.insertCell(2).innerHTML = eltTable.entryPricePerOne;
      row.insertCell(3).innerHTML = eltTable.entryTotalAmount;
      row.insertCell(4).innerHTML = `<select id=${`item_id_${i}`} class="uk-select">${selectString}</select>`;
    }

    // draw table headers
    const headers = ['Позиция', 'Количество', 'Цена за ед/кг', 'Стоимость', 'Категория учета'];
    const header = table.createTHead();
    const headerRow = header.insertRow(0);

    for (let i = 0; i < headers.length; i += 1) {
      headerRow.insertCell(i).innerText = headers[i];
    }

    // draw docs
    document.getElementById('descriptionHeader').style.display = 'block';
    document.getElementById('mergeCats').style.display = 'block';

    invoiceBlock.append(table);
  }
}
