const calculate = () => {
  const entriesLen = document.getElementsByClassName('product__entry').length;

  // create invoice items arrays
  const invoiceObjects = [];
  for (let i = 0; i < entriesLen; i += 1) {
    const totalAmountItem = parseFloat(document.querySelector(`#selectTable > tbody > tr:nth-child(${i + 1}) > td:nth-child(4)`).innerText);
    const categoryItem = document.getElementById(`item_id_${i}`).value;
    invoiceObjects.push({
      item: categoryItem,
      itemPrice: totalAmountItem,
    });
  }

  // merge by category and calculate amount
  const result = invoiceObjects.reduce((cur, val) => {
    const alreadyIn = cur.find((e) => e.item === val.item);
    if (alreadyIn) {
      // alreadyIn.itemPrice = (parseFloat(alreadyIn.itemPrice) + parseFloat(val.itemPrice));
      alreadyIn.itemPrice += val.itemPrice;
    } else {
      cur.push(val);
    }
    return cur;
  }, []);

  // show result block
  document.getElementById('result').style.display = 'block';

  // reconciliation
  let recon = document.getElementById('totalAmount').innerText;
  recon = parseFloat(recon).toFixed(2);

  // sum for doublecheck
  let sum = 0;
  result.forEach((goods) => {
    sum += goods.itemPrice;
  });
  sum = sum.toFixed(2);

  // draw block sum
  const reconciliation = document.getElementById('reconciliation');
  if (recon === sum) {
    reconciliation.innerHTML = `Сумма подсчета сходится с суммой чека ${sum}`;
    document.getElementById('reconciliation').style.color = 'green';
  } else {
    reconciliation.innerHTML = `Сумма не сходится с исходной ${sum}. Возможно проблема в округлении`;
    document.getElementById('reconciliation').style.color = 'red';
  }

  // draw final table
  const finalTable = document.createElement('table');
  finalTable.setAttribute('class', 'uk-table uk-table-striped uk-table-small uk-table-responsive');

  const resLen = result.length;
  for (let i = 0; i < resLen; i += 1) {
    const row = finalTable.insertRow(i);
    row.setAttribute('class', 'final-entry-record');
    const element = result[i];
    row.insertCell(0).innerText = element.item;
    row.insertCell(1).innerText = parseFloat(element.itemPrice.toFixed(2));
  }

  // append table
  const resultTableBlock = document.getElementById('resultTable');
  resultTableBlock.append(finalTable);
  resultTableBlock.style.display = 'block';
};
