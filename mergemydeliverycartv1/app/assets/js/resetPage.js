// reset page
function resetDescriptionInfo() {
  document.getElementById('descriptionInfo').style.display = 'none';
  document.getElementById('date').innerText = '';
  document.getElementById('seller').innerText = '';
  document.getElementById('totalAmount').innerText = '';
}

function resetDescriptionTable() {
  document.getElementById('descriptionHeader').style.display = 'none';
  document.getElementById('mergeCats').style.display = 'none';
}

function resetResult() {
  document.getElementById('reconciliation').innerHTML = '';
  document.getElementById('resultTable').innerHTML = '';
  const resultHeaders = document.querySelectorAll('#result h3');
  resultHeaders.forEach((elt) => {
    elt.style.display = 'none';
  });
  document.getElementById('selectTable').remove();
}

// composition function
function resetPage() {
  console.log('reset is here');
  resetDescriptionInfo();
  resetDescriptionTable();
  resetResult();
}
