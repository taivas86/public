<?php
/*
Шаблон страницы "Блог"
*/
?>
<?php get_header(); ?>

<div class="blog__head">
    <div class ="blog__head_header">
        <h1>Блог</h1>
    </div>
    <div class="blog__head_text">
        <p>Современное развитие технологий невозможно без обмена опытом</p>
    </div>

    <div class="blog__head_img">
        <img class="cover__img" src="/wp-content/uploads/2016/11/blogheader.jpg"/>
    </div>
</div>

<div class="gridcontainer">
    <?php
        // Grid Parameters
        $counter = 1; // Start the counter
        $grids = 3; // Grids per row
        $titlelength = 50; // Length of the post titles shown below the thumbnails

        // The Loop
        query_posts($query_string.'&cat=2&showposts=6');
        while (have_posts()) : the_post();
        // Show all columns except the right hand side column
            if ($counter != $grids) :
    ?>

    <div class="griditemleft">
        <div class="postimage">
            <a href="<?php the_permalink(); ?>"
               title="<?php the_title_attribute(); ?>"><div class="blog__post_oldfix"><?php the_post_thumbnail(); ?></div>
            </a>
        </div><!-- .postimage -->
        <h3 class="postimage-title">
                <a class="blog__links_settings"href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php
                        if (mb_strlen($post->post_title) > $titlelength) {
                            echo mb_substr(the_title($before = '', $after = '', FALSE), 0, $titlelength) . ' ...';
                        } else {
                            the_title();
                        }
                    ?>
                </a>
            </h3>
        <div class="blog__date">
                <?php the_time(' j.m.Y'); ?>
            </div>
        <div class="blog__next">
                <a href="<?php the_permalink(); ?>">Читать далее</a>
            </div>
    </div>

    <!-- end griditemleft -->
    <?php
    // Show the right hand side column
        elseif ($counter == $grids) :
    ?>
    <div class="griditemright">
        <div class="postimage">
            <a href="<?php the_permalink(); ?>"
               title="<?php the_title_attribute(); ?>"><div class="blog__post_oldfix"><?php the_post_thumbnail(); ?></div>
            </a>
        </div><!-- .postimage -->
        <h3 class="postimage-title">
            <a class="blog__links_settings" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                <?php
                    if (mb_strlen($post->post_title) > $titlelength) {
                        echo mb_substr(the_title($before = '', $after = '', FALSE), 0, $titlelength) . ' ...';
                    } else {
                        the_title();
                    }
                ?>
            </a>
        </h3>
        <div class="blog__date">
            <?php the_time(' j.m.Y'); ?>
        </div>
        <div class="blog__next">
            <a href="<?php the_permalink(); ?>">Читать далее</a>
        </div>
    </div><!-- .griditemright -->
    <div class="clear"></div>
    <?php
        $counter = 0;
        endif;
        $counter++;
        endwhile;
        wp_reset_postdata();
    ?>
</div>
<div class="gridcontainer__pad">
    <?php
    // Grid Parameters
    $counter = 1; // Start the counter
    $grids = 2; // Grids per row
    $titlelength = 50; // Length of the post titles shown below the thumbnails

    // The Loop
    query_posts($query_string.'&cat=2&showposts=6');
    while (have_posts()) : the_post();
        // Show all columns except the right hand side column
        if ($counter != $grids) :
            ?>

            <div class="griditemleft">
                <div class="postimage">
                    <a href="<?php the_permalink(); ?>"
                       title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail(); ?>
                    </a>
                </div><!-- .postimage -->
                <h3 class="postimage-title">
                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                        <?php
                        if (mb_strlen($post->post_title) > $titlelength) {
                            echo mb_substr(the_title($before = '', $after = '', FALSE), 0, $titlelength) . ' ...';
                        } else {
                            the_title();
                        }
                        ?>
                    </a>
                </h3>
                <div class="blog__date">
                    <?php the_time(' j.m.Y'); ?>
                </div>
                <div class="blog__next">
                    <a href="<?php the_permalink(); ?>">Читать далее</a>
                </div>
            </div>

            <!-- end griditemleft -->
            <?php
        // Show the right hand side column
        elseif ($counter == $grids) :
            ?>
            <div class="griditemright">
                <div class="postimage">
                    <a href="<?php the_permalink(); ?>"
                       title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail(); ?>
                    </a>
                </div><!-- .postimage -->
                <h3 class="postimage-title">
                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                        <?php
                        if (mb_strlen($post->post_title) > $titlelength) {
                            echo mb_substr(the_title($before = '', $after = '', FALSE), 0, $titlelength) . ' ...';
                        } else {
                            the_title();
                        }
                        ?>
                    </a>
                </h3>
                <div class="blog__date">
                    <?php the_time(' j.m.Y'); ?>
                </div>
                <div class="blog__next">
                    <a href="<?php the_permalink(); ?>">Читать далее</a>
                </div>
            </div><!-- .griditemright -->
            <div class="clear"></div>
            <?php
            $counter = 0;
        endif;
        $counter++;
    endwhile;
    wp_reset_postdata();
    ?>
</div>
<div style="clear:both"></div>

<div class="blog__pagination">
    <?php wp_pagenavi(); ?>
</div>

<?php get_footer(); ?>
