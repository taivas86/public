<?php
/*
Template Name: Шаблон "Услуги"
*/
?>

<?php get_header(); ?>
    <?php if (have_posts()): while (have_posts()): the_post(); ?>
        <?php the_content(); ?>
    <?php endwhile; endif; ?>
    <div class="otherservicescontact">
        <div class="otherservicescontact__cell_one">
            <h3><strong>Оставьте контактный телефон и мы свяжемся с Вами</strong></h3>
        </div>
        <div class="otherservicescontact__cell_two">
            <div class="otherservicescontact__form">
                <form method="post">
                    <div class="otherservicescontact__sumbit">
                        <?php
                            if ($_SERVER["REQUEST_METHOD"] == "POST") {

                                if (isset($_POST['username'])) $username = $_POST['username'];
                                if (isset($_POST['usermobilephone'])) $usermobile = $_POST['usermobilephone'];

                                $username = htmlspecialchars ($username);
                                $usermobile = filter_var($usermobile, FILTER_SANITIZE_NUMBER_FLOAT);

                                $username = trim($username);
                                $usermobile = trim($usermobile);

                                if (mail("odegov.sv@gmail.com", "Please recall me", "Client:".$username.". Phone: ".$usermobile))
                                {
                                    echo "Спасибо! Мы свяжемся с Вами в ближайшее время!";
                                } else {
                                    echo "При отправке сообщения возникли ошибки";
                                }
                            }
                        ?>
                    </div>
                    <div class="input-field otherservicescontact__field">
                        <p class="otherservicescontact_header">Ваше имя</p>
                        <p class="otherservicescontact__text"><input type="text" name="username" required></p>
                    </div>
                    <div class="input-field otherservicescontact__field">
                        <p class="otherservicescontact_header">Телефон</p>
                        <p class="otherservicescontact__text"><input type="text" name="usermobilephone" required pattern="^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$"></p>
                    </div>
                    <div class="otherservicescontact__button">
                        <button class="btn waves-effect waves-light deep-orange darken-1" type="submit" name="action">
                            Отправить
                            <!--noindex--><i class="material-icons right">send</i><!--/noindex-->
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </div>
<?php get_footer(); ?>
