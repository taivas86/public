<!doctype html>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <title><?php wp_title('«', true, 'right'); ?> <?php bloginfo('name'); ?></title>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <script src="https://code.jquery.com/jquery-3.6.3.min.js" integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" crossorigin="anonymous"></script>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <div class="contact__header">
        <div class="contact__mail">
            <!--noindex-->
                <div class="contact__icon">
                    <i class="small material-icons">email</i>
                </div>
            <!--/noindex-->
            <div class="contact__mail_send">
                <a href="mailto:mail@outlooker.ru?subject=Вопрос по сайту"><u>mail@outlooker.ru</u></a>
            </div>
        </div>

        <div class="contact__phone">
            <div class = "footer__blocks footer__blocks_messenger">
                    <a href="https://wa.me/79218636930?text=Сообщение%20с%20сайта%20Outlooker" class="footer__messenger_main footer__messenger_whatsapp whatsapp__header"></a>
                    <div class="divider__header"></div>
                    <a href="https://t.me/sodegov" class="footer__messenger_main footer__messenger_telegram whatsapp__header"></a>
                    <!-- <div class="divider__header"></div>
                    <a href="viber://chat?number=79218636930>Сообщение с сайта outlooker" class="footer__messenger_main footer__messenger_viber whatsapp__header"></a> -->
            </div>
        </div>

        <div class="contact__form">
            <!--noindex-->
                <div class="contact__icon">
                    <i class="small material-icons">forum</i>
                </div>
            <!--/noindex-->
            <div class="contact__form_text">
                <p><u><a href="/contacts/">Задать вопрос</u></p>
            </div>
        </div>
    </div>
    <nav>
        <div class="nav-wrapper">
            <a href="/" class="brand-logo">Outlooker.ru</a>
            <a href="#" data-activates="mobile-demo" class="button-collapse nav__button"> <!--noindex--><i class="material-icons">menu</i> <!--/noindex--></a>
            <ul id="nav-mobile" class="hide-on-med-and-down">
                <li><a href="/sozdanie-saitov-v-sankt-peterburge/">Заказать сайт</a></li>
                <li><a href="/uslugi/">Услуги</a></li>
                <li><a href="/portfolio-vypolnennye-raboty/">Портфолио</a></li>
                <li><a href="/blog/">Блог</a></li>
                <li><a href="/contacts/">Контакты</a></li>
            </ul>
            <ul id="mobile-demo" class="side-nav">
                <li><p class="nav__mobile_logo">Outlooker.ru</p><li>
                <li><a class="nav__mobile_link" href="/">Главная</a></li>
                <li><a class="nav__mobile_link" href="/sozdanie-saitov-v-sankt-peterburge/">Заказать сайт</a></li>
                <li><a class="nav__mobile_link" href="/uslugi/">Услуги</a></li>
                <li><a class="nav__mobile_link" href="/portfolio-vypolnennye-raboty/">Портфолио</a></li>
                <li><a class="nav__mobile_link" href="/blog/">Блог</a></li>
                <li><a class="nav__mobile_link" href="/contacts/">Контакты</a></li>
            </ul>
        </div>
        <script>
            $(".button-collapse").sideNav();
        </script>
    </nav>
