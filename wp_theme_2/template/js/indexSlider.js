$(document).ready(function () {
    var swiper = new Swiper('.swiper-container', {
        centeredSlides: true,
        speed:3500,
        autoplay: {
          delay: 3000,
          disableOnInteraction: false,
        },
        loop: true
    });
});

