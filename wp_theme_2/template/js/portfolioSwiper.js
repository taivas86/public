var swiper = new Swiper('#portfolioSwiper', {
    slidesPerView: 3,
    slidesPerColumn: 2,
    slidesPerGroup:3,
    grabCursor: true,
    spaceBetween: 30,
    pagination: {
    el: '.swiper-pagination',
       clickable: true,
     },
});
