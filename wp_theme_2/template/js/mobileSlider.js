//Слайдер мобильного портфолио
$(document).ready(function() {
    $("#ourworksmobile").owlCarousel({
        slideSpeed : 0,
        paginationSpeed : 400,
        singleItem:true,
        mouseDrag: true,
        navigation:true,
        navigationText: ["Назад","Вперед"],
        pagination:false
    });
});
