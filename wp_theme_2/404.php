<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <title><?php wp_title('«', true, 'right'); ?> <?php bloginfo('name'); ?></title>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <link rel='stylesheet' media='screen and (min-width: 0px) and (max-width: 600px)' href='<?php bloginfo('template_url'); ?>/template/css/phone.css'/>
    <link rel='stylesheet' media='screen and (min-width: 601px) and (max-width: 992px)' href='<?php bloginfo('template_url'); ?>/template/css/pad.css'/>
    <link rel='stylesheet' media='screen and (min-width: 993px) and (max-width: 5000px)' href='<?php bloginfo('template_url'); ?>/template/css/desktop.css'/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300' rel='stylesheet' type='text/css'>
    <link rel="icon" href="img/favicon.png">
</head>
<body>
<div class="downbox">
    <div class="downbox__text">
        <div class="downbox__text_head">
            <a href="/">Outlooker.ru</a>
        </div>
        <div class="downbox__text_error">
            <p>Ошибка 404. Запрашиваемая страница перемещена или удалена</p>
        </div>
        <div class="downbox__error_help">
            <p>Не надо переживать!</p>
            <p>Данная страница недоступна, но у нас есть еще 100500 интересных страниц</p>
        </div>
        <div class="downbox__main_page">
            <p>Начните с главной - <a class="downbox__links"href="index.php">Outlooker.ru</a>
        </div>
        <div class="downbox__otherpage">
            <p>Или просмотрите остальные разделы:</p>
        </div>
        <div class="dropdown__otherpage_list">
            <a href="/sozdanie-saitov-v-sankt-peterburge/">Заказать веб-сайт</a>
            <a href="/uslugi/">Услуги</a>
            <!--<a href="/portfolio-vypolnennye-raboty/">Портфолио</a>-->
            <a href="/blog/">Блог</a>
            <a href="/contacts/">Контакты</a>
        </div>
    </div>
</div>
</body>
</html>