<?php
/*
Template Name: Шаблон "Контакты"
*/
?>

<?php get_header(); ?>
    <div class="contactpage__head">
        <div class="contactpage__header">
            <h1>Контакты</h1>
        </div>
        <div class="contactpage__header_text">
            <p>Outlooker.ru - веб-сайты в Москве и Санкт-Петербурге</p>
        </div>
        <div class="contactpage__image">
            <img class="cover__img" src="/wp-content/uploads/2016/11/contact.png">
        </div>
    </div>
    <div class="contactpage__info">
        <div class="contactpage__info_header">
            <h3>Контакты для найма и вопросов</h3>
        </div>
        <div class="contactpage__info_data contactpage__date_margin">
            <div class="contactpage__info_line">
                <div class="contactpage__info_item">Телефон:</div>
                <div class="contactpage__info_phone">
                    <div><a href="tel:+79218636930">+7 (921) 863-69-30</a></div>
                </div>
            </div>
        </div>
        <div class="contactpage__info_data contactpage__date_margin ">
            <div class="contactpage__info_line">
                <div class="contactpage__info_item">Электронная почта:</div>
                <div class="contactpage__info_phone">
                    <div> <a href="mailto:mail@outlooker.ru?subject=Вопрос по сайту"><u>mail@outlooker.ru</u></a></div>
                </div>
            </div>
        </div>
        <div class="contactpage__info_data contacts__messenger_grid">
            <div class="contactpage__info_line">
                <div class="contactpage__info_item">Мессенджеры:</div>
                    <div class="contact__phone contactpage__messenger">
                        <div class = "footer__blocks footer__blocks_messenger mobile__contacts_grid">
                                <a href="https://wa.me/79218636930?text=Сообщение%20с%20сайта%20Outlooker" class="footer__messenger_main footer__messenger_whatsapp contacticon__color"></a>
                                <div class="divider__header divider__contacts"></div>
                                <a href="https://t.me/sodegov" class="footer__messenger_main footer__messenger_telegram contacticon__color"></a>
                                <!-- <div class="divider__header divider__contacts"></div>
                                <a href="viber://chat?number=79218636930>Сообщение с сайта outlooker" class="footer__messenger_main footer__messenger_viber contacticon__color"></a> -->
                        </div>
                    </div>
            </div>
        </div>

    </div>
    <?php if (have_posts()): while (have_posts()): the_post(); ?>
        <?php the_content(); ?>
    <?php endwhile; endif; ?>

<?php get_footer(); ?>
