<?php
/* Template Name: Шаблон главной страницы */
?>

<?php get_header(); ?>
    <div class="seoblock__mobile">
        <p>Мы занимаемся созданием сайтов, интернет магазинов и сопровождением сайтов в Москве и Санкт-Петербурге с 2009 года</p>
    </div>
    <div class="slider">
        <div class="slidertext"></div>
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide"><img class="cover__img" src="/wp-content/uploads/2021/05/banner21.jpg" alt="Создание сайта в Москве Санкт-Петербурге"></div>
                <!-- <div class="swiper-slide"><img src="/wp-content/uploads/2019/09/banner2-19.jpg" alt="Разработка и продвижение сайтов"/></div> -->
                <!-- <div class="swiper-slide"><img src="/wp-content/uploads/2019/09/banner4-19.jpg" alt="Интернет-магазин Москве и СПб"/></div> -->
            </div>
        </div>
        <div class="slidermessage">
            <div class="slidermessage__header">
                <h1>Разработка сайта, интернет-магазина, сопровождение сайта в Москве и СПб</h1>
            </div>
            <div class="slidermessage__text">
                <p>Наша команда собралась в 2009 году. Основным направлением нашей деятельности является разработка сайтов, создание интернет магазинов и техническая поддержка</p>
            </div>
            <div class="slidermessage__button">
                <a class="red accent-4 waves-effect waves-light btn-large button__spec" href="/sozdanie-saitov-v-sankt-peterburge/">Узнайте больше</a>
            </div>
        </div>
    </div>
        <?php if (have_posts()):
    while (have_posts()):
        the_post(); ?>
            <?php the_content(); ?>
        <?php
    endwhile;
endif; ?>
    </section>

<div class="main__page">
    <h2 class="mainpage__header">Свежие посты из блога</h2>
    <div class="test__blog">
        <div class="gridcontainer gridcontainer__mainpage">
    <?php
// Grid Parameters
$counter = 1; // Start the counter
$grids = 3; // Grids per row
$titlelength = 50; // Length of the post titles shown below the thumbnails

// The Loop
query_posts($query_string . '&cat=2&showposts=3');
while (have_posts()):
    the_post();
    // Show all columns except the right hand side column
    if ($counter != $grids):
?>

            <div class="griditemleft main__blog_fix">
                <div class="postimage">
                    <a href="<?php the_permalink(); ?>"
                       title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail(); ?>
                    </a>
                </div><!-- .postimage -->
                <h3 class="postimage-title">
                    <a class="blog__links_settings"href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                        <?php
        if (mb_strlen($post->post_title) > $titlelength) {
            echo mb_substr(the_title($before = '', $after = '', FALSE), 0, $titlelength) . ' ...';
        }
        else {
            the_title();
        }
?>
                    </a>
                </h3>
                <div class="blog__date">
                    <?php the_time(' j.m.Y'); ?>
                </div>
                <div class="blog__next">
                    <a href="<?php the_permalink(); ?>">Читать далее</a>
                </div>
            </div>

            <!-- end griditemleft -->
            <?php
    // Show the right hand side column
    elseif ($counter == $grids):
?>
            <div class="griditemright">
                <div class="postimage">
                    <a href="<?php the_permalink(); ?>"
                       title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail(); ?>
                    </a>
                </div><!-- .postimage -->
                <h3 class="postimage-title">
                    <a class="blog__links_settings" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                        <?php
        if (mb_strlen($post->post_title) > $titlelength) {
            echo mb_substr(the_title($before = '', $after = '', FALSE), 0, $titlelength) . ' ...';
        }
        else {
            the_title();
        }
?>
                    </a>
                </h3>
                <div class="blog__date">
                    <?php the_time(' j.m.Y'); ?>
                </div>
                <div class="blog__next">
                    <a href="<?php the_permalink(); ?>">Читать далее</a>
                </div>
            </div><!-- .griditemright -->
            <div class="clear"></div>
            <?php
        $counter = 0;
    endif;
    $counter++;
endwhile;
wp_reset_postdata();
?>
</div>
    </div>
    <div class="gridcontainer__pad gridcontainer__mainpage_pad">
        <?php
// Grid Parameters
$counter = 1; // Start the counter
$grids = 2; // Grids per row
$titlelength = 50; // Length of the post titles shown below the thumbnails

// The Loop
query_posts($query_string . '&cat=2&showposts=2');
while (have_posts()):
    the_post();
    // Show all columns except the right hand side column
    if ($counter != $grids):
?>

                <div class="griditemleft">
                    <div class="postimage">
                        <a href="<?php the_permalink(); ?>"
                           title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail(); ?>
                        </a>
                    </div><!-- .postimage -->
                    <h3 class="postimage-title">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                            <?php
        if (mb_strlen($post->post_title) > $titlelength) {
            echo mb_substr(the_title($before = '', $after = '', FALSE), 0, $titlelength) . ' ...';
        }
        else {
            the_title();
        }
?>
                        </a>
                    </h3>
                    <div class="blog__date">
                    </div>
                    <div class="blog__next">
                        <a href="<?php the_permalink(); ?>">Читать далее</a>
                    </div>
                </div>

                <!-- end griditemleft -->
                <?php
    // Show the right hand side column
    elseif ($counter == $grids):
?>
                <div class="griditemright">
                    <div class="postimage">
                        <a href="<?php the_permalink(); ?>"
                           title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail(); ?>
                        </a>
                    </div><!-- .postimage -->
                    <h3 class="postimage-title">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                            <?php
        if (mb_strlen($post->post_title) > $titlelength) {
            echo mb_substr(the_title($before = '', $after = '', FALSE), 0, $titlelength) . ' ...';
        }
        else {
            the_title();
        }
?>
                        </a>
                    </h3>
                    <div class="blog__next">
                        <a href="<?php the_permalink(); ?>">Читать далее</a>
                    </div>
                </div><!-- .griditemright -->
                <div class="clear"></div>
                <?php
        $counter = 0;
    endif;
    $counter++;
endwhile;
wp_reset_postdata();
?>
    </div>
</div>
<?php get_footer(); ?>
