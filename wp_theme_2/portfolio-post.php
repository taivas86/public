<?php
/*
Template Name: Post Portfolio
Template Post Type: post, funerals
*/

?>

<?php get_header(); ?>
<div class="page__content portfolio__post_mobile">
    <?php if (have_posts()): while (have_posts()): the_post(); ?>
        <div class="page__content_header portfolio__post_header">
            <h1><?php the_title(); ?></h1>
        </div>
        <div class="page__content_text portflio__mobile_block">
            <?php the_content(); ?>
        </div>
    <?php endwhile; endif; ?>
</div>
<!--VK button connect-->
<script type="text/javascript" src="//vk.com/js/api/openapi.js?146"></script>

<script type="text/javascript">
    VK.init({apiId: 6116646, onlyWidgets: true});
</script>
<!--end-->
<div class="page__sharing">
    <p class="footer__share_text">Поделитесь с друзьями и коллегами:</p>
    <span id="vk_like"></span>
    <script type="text/javascript">
        VK.Widgets.Like("vk_like", {type: "mini", height: 30});
    </script>

    <span class='st_facebook_large' displayText='Facebook'></span>
    <span class='st_linkedin_large' displayText='LinkedIn'></span>
    <span class='st_email_large' displayText='Email'></span>
    <span class='st_print_large' displayText='Print'></span>
    <script type="text/javascript">var switchTo5x=true;</script>
    <script type="text/javascript">stLight.options({publisher: "8267c538-86a9-4b5d-91a1-a3837edc971e", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
</div>
<div class="otherservicescontact">
    <div class="otherservicescontact__cell_one">
        <h3><strong>Оставьте контактный телефон и мы свяжемся с Вами</strong></h3>
    </div>
    <div class="otherservicescontact__cell_two">
        <div class="otherservicescontact__form">
            <form method="post">
                <div class="otherservicescontact__sumbit">
                    <?php
                    if ($_SERVER["REQUEST_METHOD"] == "POST") {

                        if (isset($_POST['username'])) $username = $_POST['username'];
                        if (isset($_POST['usermobilephone'])) $usermobile = $_POST['usermobilephone'];;

                        $username = htmlspecialchars ($username);
                        $usermobile = filter_var($usermobile, FILTER_SANITIZE_NUMBER_FLOAT);

                        $username = trim($username);
                        $usermobile = trim($usermobile);

                        if (mail("odegov.sv@gmail.com", "Please recall me", "Client:".$username.". Phone: ".$usermobile))
                        {
                            echo "Спасибо! Мы свяжемся с Вами в ближайшее время!";
                        } else {
                            echo "При отправке сообщения возникли ошибки";
                        }
                    }
                    ?>
                </div>
                <div class="input-field otherservicescontact__field">
                    <p class="otherservicescontact_header">Ваше имя</p>
                    <p class="otherservicescontact__text"><input type="text" name="username" required></p>
                </div>
                <div class="input-field otherservicescontact__field">
                    <p class="otherservicescontact_header">Телефон</p>
                    <p class="otherservicescontact__text"><input type="text" name="usermobilephone" required pattern="^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$"></p>
                </div>
                <div class="otherservicescontact__button">
                    <button class="btn waves-effect waves-light deep-orange darken-1" type="submit" name="action">
                        Отправить
                        <!--noindex--><i class="material-icons right">send</i><!--/noindex-->
                    </button>
                </div>

            </form>
        </div>
    </div>
</div>
</div>
<?php get_footer(); ?>
