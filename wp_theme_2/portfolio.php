<?php
/*
Template Name: Шаблон "Портфолио"
*/
?>

<?php get_header(); ?>
<div class="seoblock seoblock__works">
    <div class="seoblock__header">
        <h1>Outlooker - cоздание сайта в Москве и Санкт-Петербурге</h1>
    </div>
    <div class="seoblock__text">
        <p>Мы занимаемся созданием сайтов в Москве и СПб с 2009 года. Оказываем комплексные услуги
по созданию сайтов, начиная от простых лендинг пейдж и открытия интернет магазина с нуля, заканчивая веб приложениями и CRM. Мы не только создаем новые проекты, но и занимаемся сопровождением и поддержкой сайтов. Нам необходим Ваш успех и долговременное сотрудничество с нами </p>
    </div>
</div>

<?php if (have_posts()): while (have_posts()): the_post(); ?>
    <?php the_content(); ?>
<?php endwhile; endif; ?>

<?php get_footer(); ?>
