<?php wp_footer(); ?>
<div class ="footer">
    <script>
        $(document).ready(function(){
            // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
            $('.modal').modal();
        });
    </script>
    <div class="footer__share footer__flex ">
        <div class="footer__contact">
            <div class="footer__logo">
                <h4>Контакты</h4>
            </div>
            <div class = "footer__blocks">
                <p>Телефон: +7 (921) 863-69-30</p>
            </div>
            <div class = "footer__blocks">
                <p>e-mail: <a href="mailto: mail@outlooker.ru"> mail@outlooker.ru</a></p>
            </div>
            <div class = "footer__blocks footer__blocks_messenger">
                <a href="https://wa.me/79218636930?text=Сообщение%20с%20сайта%20Outlooker" class="footer__messenger_main footer__messenger_whatsapp"></a>
                <a href="https://t.me/sodegov" class="footer__messenger_main footer__messenger_telegram"></a>
                <!-- <a href="viber://chat?number=79218636930>Сообщение с сайта outlooker" class="footer__messenger_main footer__messenger_viber"></a> -->
            </div>
            <div class = "footer__blocks">
                <p>2009 - <?php echo date("Y"); ?> &copy; разработано Outlooker.ru</p>
            </div>
        </div>
    </div>
    <div class="footer__share footer__flex ">
        <div class="footer__contact">
            <div class="footer__logo">
                <h4>Outlooker.ru</h4>
            </div>
            <div class="footer_block_second">
                <div class = "footer__blocks">
                    <a href="#modal99">Скачать прайс-лист</a>
                    <div id="modal99" class="modal">
                        <div class="modal-content footer__modal_price">
                            <h4>Загрузить прайс-лист</h4>
                            <div class="footer__modal_links">
                                <div class="footer__modal_link footer__price_first">
                                    <div class="footer__modal_icon"><!--noindex--><i class="small material-icons">view_list</i><!--/noindex--></div>
                                    <div><a href="/prices/priceOutlooker.xls">В формате MS Excel</a></div>
                                </div>
                                <div class="footer__modal_link">
                                    <div class="footer__modal_icon"><!--noindex--><i class="small material-icons">view_list</i><!--/noindex--></div>
                                    <div><a target="_blank" href="/prices/priceOutlooker.pdf">В формате PDF</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="#!" class=" modal-action modal-close waves-effect  btn-flat red accent-4 modal__price_btn">Закрыть</a>
                        </div>
                    </div>
                </div>
                <div class = "footer__blocks">
                    <a href="/sozdanie-saitov-v-sankt-peterburge/">Заказать сайт</a>
                </div>
                <div class = "footer__blocks">
                    <a href="/uslugi/">Услуги</a>
                </div>
                <div class = "footer__blocks">
                    <a href="/portfolio-vypolnennye-raboty/">Портфолио</a>
                </div>
                <div class = "footer__blocks">
                    <a href="/blog/">Блог</a>
                </div>
                <div class = "footer__blocks">
                    <a href="/contacts/">Контакты</a>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__share footer__flex ">
        <div class="footer__contact">
            <div class="footer__logo">
                <h4>Информация</h4>
            </div>
            <p>Предложения на сайте носят информационный характер и не являются публичной офертой</p>
            <p><a href="/oferta/">Оферта</a></p>
            <p>Мы принимаем:</p>
            <div class="footer__paysystems">
                <div class="paysystem__img">
                    <img src="/wp-content/themes/outlookerTheme/template/img/paysystems/visa.svg">
                </div>
                <div class="paysystem__img">
                    <img src="/wp-content/themes/outlookerTheme/template/img/paysystems/mastercard.svg">
                </div>
                <div class="paysystem__img paysystem__img_paypal">
                    <img src="/wp-content/themes/outlookerTheme/template/img/paysystems/paypal-2.svg">
                </div>            
            </div>
        </div>
    </div>
</div>
    <div class="footer__mobile">
        <div class="contact__phone">
            <div class = "footer__blocks footer__blocks_messenger">
                    <a href="https://wa.me/79218636930?text=Сообщение%20с%20сайта%20Outlooker" class="footer__messenger_main footer__messenger_whatsapp whatsapp__header"></a>
                    <div class="divider__header"></div>
                    <a href="tg://resolve?domain=sodegov" class="footer__messenger_main footer__messenger_telegram whatsapp__header"></a>
                    <div class="divider__header"></div>
                    <!-- <a href="viber://chat?number=79218636930>Сообщение с сайта outlooker" class="footer__messenger_main footer__messenger_viber whatsapp__header"></a> -->
            </div>
        </div>
    </div>
    <script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>
    <!-- Yandex.Metrika counter --> <script type="text/javascript" > (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date(); for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }} k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym"); ym(41463369, "init", { clickmap:true, trackLinks:true, accurateTrackBounce:true }); </script> <noscript><div><img src="https://mc.yandex.ru/watch/41463369" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
    </body>
    </html>
