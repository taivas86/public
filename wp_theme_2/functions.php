<?php
//Outlooker styles

function materialize_enqueue_styles() {
    wp_enqueue_style( 'materialize-outlooker-style');
    wp_register_style('materialize-style',  get_template_directory_uri() . '/template/css/materialize.css');
    wp_enqueue_style( 'materialize-style');
}
add_action('wp_enqueue_scripts', 'materialize_enqueue_styles');

function phone_enqueue_styles() {
    wp_enqueue_style( 'phone-outlooker-style');
    wp_register_style('phone-style',  get_template_directory_uri() . '/template/css/phone.css');
    wp_enqueue_style( 'phone-style');
}
add_action('wp_enqueue_scripts', 'phone_enqueue_styles');

function pad_enqueue_styles() {
    wp_enqueue_style( 'pad-outlooker-style');
    wp_register_style('pad-style',  get_template_directory_uri() . '/template/css/pad.css');
    wp_enqueue_style( 'pad-style');
}
add_action('wp_enqueue_scripts', 'pad_enqueue_styles');

function desktop_enqueue_styles() {
    wp_enqueue_style( 'desktop-outlooker-style');
    wp_register_style('desktop-style',  get_template_directory_uri() . '/template/css/desktop.css');
    wp_enqueue_style( 'desktop-style');
}
add_action('wp_enqueue_scripts', 'desktop_enqueue_styles');

function icons_enqueue_styles() {
    wp_enqueue_style( 'icons-outlooker-style', get_stylesheet_uri());
    wp_register_style('icons-style', 'https://fonts.googleapis.com/icon?family=Material+Icons');
    wp_enqueue_style( 'icons-style');
}
add_action('wp_enqueue_scripts', 'icons_enqueue_styles');

function fonts_enqueue_styles() {
    wp_enqueue_style( 'fonts-outlooker-style', get_stylesheet_uri());
    wp_register_style('fonts-style', 'https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i&amp;subset=cyrillic');
    wp_enqueue_style( 'fonts-style');
}
add_action('wp_enqueue_scripts', 'fonts_enqueue_styles');

function portfoliopost_enqueue_styles() {
    wp_enqueue_style( 'portfoliopost-outlooker-style');
    wp_register_style('portfoliopost-style',  get_template_directory_uri() . '/template/css/portfolio-screens.css');
    wp_enqueue_style( 'portfoliopost-style');
}
add_action('wp_enqueue_scripts', 'portfoliopost_enqueue_styles');


//Outlooker scripts
function materialize_enqueue_scripts () {
    wp_register_script('script-materialize-outlooker', get_template_directory_uri() . '/template/js/materialize.min.js');
    wp_enqueue_script('script-materialize-outlooker');
}
add_action('wp_enqueue_scripts', 'materialize_enqueue_scripts');

function animateList_enqueue_scripts () {
    wp_register_script('animateList-outlooker', get_template_directory_uri() . '/template/js/animateList.js');
    wp_enqueue_script('animateList-outlooker');
}
add_action('wp_enqueue_scripts', 'animateList_enqueue_scripts');

function modalOtherUslugi_enqueue_scripts () {
    wp_register_script('modalOtherUslugi-outlooker', get_template_directory_uri() . '/template/js/modalOtherUslugi.js');
    wp_enqueue_script('modalOtherUslugi-outlooker');
}
add_action('wp_enqueue_scripts', 'modalOtherUslugi_enqueue_scripts');

// Remove Wordpress auto <p> styling
remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );


////Remove JS Contact Form 7 on each page
add_filter( 'wpcf7_load_js', '__return_false' );
//add_filter( 'wpcf7_load_css', '__return_false' );

//Add Post Picture
add_theme_support( 'post-thumbnails' );

//Disable Hueten'berg
add_filter('use_block_editor_for_post', '__return_false');

//autoupdate plugins && core
add_filter( 'auto_update_plugin', '__return_true' );
define( 'WP_AUTO_UPDATE_CORE', true );

//Disable yoast scheme generator
add_filter( 'wpseo_json_ld_output', '__return_false' );


//Disable emojes and other stuff
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10);
remove_action('wp_head', 'parent_post_rel_link', 10);
remove_action('wp_head', 'adjacent_posts_rel_link', 10);

// REMOVE WP EMOJI
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

//disable heartbeat
add_action( 'init', 'my_deregister_heartbeat', 1 );
function my_deregister_heartbeat() {
    global $pagenow;

    if ( 'post.php' != $pagenow && 'post-new.php' != $pagenow )
        wp_deregister_script('heartbeat');
}

define('WP_HOME', 'https://outlooker.ru');   //add your website URL
define('WP_SITEURL', 'https://outlooker.ru');


//Yandex duplicate title error for paged 
function changeYoastPaginationTitle($desc)
{
    global $wp;
    $currentUrl = home_url($wp->request);
    $page = preg_replace('/\D/', '', $currentUrl);
    $isPageTwo = ($page >= 2);

    if ($isPageTwo && strstr($currentUrl, 'page')) {
        $desc = $desc . " - страница {$page}";
    }

    return $desc;

}

add_filter('wpseo_metadesc', 'changeYoastPaginationTitle', 10, 2);
