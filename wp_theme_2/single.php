 <?php
/*
Template Name: Шаблон поста для "Новости"
*/
?>

<?php get_header(); ?>
	<div class="page__content">
	    <?php if (have_posts()): while (have_posts()): the_post(); ?>
		<div class="page__content_header">
			<h1><?php the_title(); ?></h1>
		</div>
        <div class="page__content_text">
	        <?php the_content(); ?>
        </div>
	    <?php endwhile; endif; ?>
	</div>
    <!--VK button connect-->
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?146"></script>

    <script type="text/javascript">
        VK.init({apiId: 6116646, onlyWidgets: true});
    </script>
    <!--end-->
    <div class="page__sharing">
        <div class="page__sharing_day">
            <?php 
    	        echo "Запись опубликована:  ".$createDate = get_the_date('d.m.Y');
    	    ?>
	    </div>
        <p class="footer__share_text">Поделитесь с друзьями и коллегами:</p>
        <span id="vk_like"></span>
        <script type="text/javascript">
            VK.Widgets.Like("vk_like", {type: "mini", height: 30});
        </script>

         <span class='st_facebook_large' displayText='Facebook'></span>
         <span class='st_linkedin_large' displayText='LinkedIn'></span>
         <span class='st_email_large' displayText='Email'></span>
         <span class='st_print_large' displayText='Print'></span>
         <script type="text/javascript">var switchTo5x=true;</script>
         <script type="text/javascript">stLight.options({publisher: "8267c538-86a9-4b5d-91a1-a3837edc971e", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
    </div>
 </div>
<?php get_footer(); ?>
